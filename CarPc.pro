EMPLATE = app
TARGET = CarPc
QT += core \
    gui \
    phonon \
    sql \
    xml
HEADERS += gui/cp_dlgNavit.h \
    multimedia/cp_MediaPlayList.h \
    basis/cp_Application.h \
    basis/cp_SystemControl.h \
    gui/cp_dlgSettingMenu.h \
    gui/cp_MusikBibliothekMenu.h \
    gui/cp_dlgMusikMenu.h \
    gui/cp_dlgMainMenu.h \
    gui/cp_dlgVideoMenu.h \
    gui/cp_dlgStdMenu.h \
    gui/cp_videowidget.h \
    gui/cp_dlgVideo.h \
    multimedia/cp_ID3ENCODE.h \
    multimedia/cp_MediaPlayer.h \
    multimedia/cp_MediaBibliothek.h \
    multimedia/cp_ID3TAGReader.h \
    multimedia/cp_MediaBibliothekSyncThread.h \
    gui/cp_dlgconfig.h \
    gui/ccarpc_dlgMain.h \
    basis/CCARPCDBConnection.h \
    gui/ccarpcstyle.h \
    gui/ccarpcplaylist.h \
    gui/cmusikselect.h \
    file/CCARPCFileFinder.h \
    gui/CCARPCGuiMediaController.h \
    basis/CLog.h \
    basis/CSettings.h \
    basis/CSettingsException.h \
    basis/CCARPCExcetpion.h \
    gui/ccarpctouchkeyboard_context.h \
    gui/ccarpctouchkeyboard.h
SOURCES += gui/cp_dlgNavit.cpp \
    multimedia/cp_MediaPlayList.cpp \
    basis/cp_Application.cpp \
    basis/cp_SystemControl.cpp \
    gui/cp_dlgSettingMenu.cpp \
    gui/cp_MusikBibliothekMenu.cpp \
    gui/cp_dlgMusikMenu.cpp \
    gui/cp_dlgMainMenu.cpp \
    gui/cp_dlgVideoMenu.cpp \
    gui/cp_dlgStdMenu.cpp \
    gui/cp_videowidget.cpp \
    gui/cp_dlgVideo.cpp \
    multimedia/cp_MediaPlayer.cpp \
    multimedia/cp_MediaBibliothek.cpp \
    multimedia/cp_ID3TAGReader.cpp \
    multimedia/cp_MediaBibliothekSyncThread.cpp \
    gui/cp_dlgconfig.cpp \
    gui/ccarpc_dlgMain.cpp \
    basis/CCARPCDBConnection.cpp \
    gui/ccarpcstyle.cpp \
    gui/ccarpcplaylist.cpp \
    gui/cmusikselect.cpp \
    file/CCARPCFileFinder.cpp \
    gui/CCARPCGuiMediaController.cpp \
    basis/CLog.cpp \
    basis/CSettings.cpp \
    basis/CCARPCExcetpion.cpp \
    gui/ccarpctouchkeyboard_context.cpp \
    gui/ccarpctouchkeyboard.cpp \
    main.cpp
FORMS += gui/cp_dlgVideoSelect.ui \
    gui/cp_dlgVideo.ui \
    gui/cp_dlgconfig.ui \
    gui/cccarpc_main_dialog.ui \
    gui/cccarpc_dlgMain.ui \
    gui/ccarpcsecurity.ui \
    gui/test.ui \
    gui/ccarpcplaylist.ui \
    gui/cmusikselect.ui
RESOURCES += 
unix:LIBS += /usr/lib/libid3.a
win32:LIBS += id3Win/lib/libid3.a
win32:INCLUDEPATH += id3Win/include/