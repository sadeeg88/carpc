/*
 * CCARPCDBConnection.h
 *
 *  Created on: 20.07.2010
 *      Author: Sascha
 */
#include <QtSql/qsqldatabase.h>
#include <QtSql/qsqlerror.h>
#include "../basis/CSettings.h"

#ifndef CCARPCDBCONNECTION_H_
#define CCARPCDBCONNECTION_H_

class CCARPCDBConnection : public QSqlDatabase {
public:
	CCARPCDBConnection();
	virtual ~CCARPCDBConnection();


};

#endif /* CCARPCDBCONNECTION_H_ */
