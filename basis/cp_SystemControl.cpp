/*
 * cp_SystemControl.cpp
 *
 *  Created on: 18.04.2011
 *      Author: Sascha
 */

#include "cp_SystemControl.h"

#ifdef Q_WS_X11
#include <signal.h>
#include <sys/socket.h>
#endif
//include <../basis/CLog.h>

cp_SystemControl* cp_SystemControl::instance = NULL;
#ifdef Q_WS_X11
int cp_SystemControl::sighupFd[2] ={ 0};
int cp_SystemControl::sigtermFd[2] ={ 0};
#endif

cp_SystemControl::cp_SystemControl(QObject *parent, char *name)
 :QObject(parent)
{
#ifdef Q_WS_X11
  if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sighupFd))
     qFatal("Couldn't create HUP socketpair");

  if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sigtermFd))
     qFatal("Couldn't create TERM socketpair");
  snHup = new QSocketNotifier(sighupFd[1], QSocketNotifier::Read, this);
  connect(snHup, SIGNAL(activated(int)), this, SLOT(handleSigHup()));
  snTerm = new QSocketNotifier(sigtermFd[1], QSocketNotifier::Read, this);
  connect(snTerm, SIGNAL(activated(int)), this, SLOT(handleSigTerm()));
#endif
}

cp_SystemControl::~cp_SystemControl()
{
  // TODO Auto-generated destructor stub
}

cp_SystemControl*
cp_SystemControl::getInstance()
{
  if (!instance)
    instance = new cp_SystemControl();
  return instance;
}

void
cp_SystemControl::Destroy()
{
  // static
  if (instance)
    {
      delete instance;
      instance = NULL;
    }
}



#ifdef Q_WS_X11
void cp_SystemControl::hupSignalHandler(int)
{
    char a = 1;
    ::write(sighupFd[0], &a, sizeof(a));
}

void cp_SystemControl::termSignalHandler(int)
{
    char a = 1;
    ::write(sigtermFd[0], &a, sizeof(a));
}


void cp_SystemControl::handleSigTerm()
{
    snTerm->setEnabled(false);
    char tmp;
    ::read(sigtermFd[1], &tmp, sizeof(tmp));

    // do Qt stuff
    emit SystemTerm();

    snTerm->setEnabled(true);
}

void cp_SystemControl::handleSigHup()
{
    snHup->setEnabled(false);
    char tmp;
    ::read(sighupFd[1], &tmp, sizeof(tmp));

    // do Qt stuff

    snHup->setEnabled(true);
}

int setup_unix_signal_handlers()
 {
     struct sigaction hup, term;

     hup.sa_handler = cp_SystemControl::hupSignalHandler;
     sigemptyset(&hup.sa_mask);
     hup.sa_flags = 0;
     hup.sa_flags |= SA_RESTART;

     if (sigaction(SIGHUP, &hup, 0) > 0)
        return 1;

     term.sa_handler = cp_SystemControl::termSignalHandler;
     sigemptyset(&term.sa_mask);
     term.sa_flags |= SA_RESTART;

     if (sigaction(SIGTERM, &term, 0) > 0)
        return 2;

     return 0;
 }
#endif

