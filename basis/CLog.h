/*
 * CLog.h
 *
 *  Created on: 15.11.2009
 *      Author: Sascha
 */

#ifndef CLOG_H_
#define CLOG_H_


enum CLogLevel
{
	LOG_ERRORS,
	LOG_WARNINGS,
	LOG_INFOS,
	LOG_DEBUGWARNING,
	LOG_DEBUGDEVELOP
};

class CLog {
private:
	static CLog * instance;
	CLog();
	virtual ~CLog();
	QString m_LogDateiPath;
	int m_iLogLevel;
public:
   static CLog * getInstance();
   static void Destroy();
   void Log(QString Message,int LogLevel);
};

#endif /* CLOG_H_ */
