/*
 * CCARPCDBConnection.cpp
 *
 *  Created on: 20.07.2010
 *      Author: Sascha
 */

#include "CCARPCDBConnection.h"

CCARPCDBConnection::CCARPCDBConnection()
: QSqlDatabase("QMYSQL")
{
	setHostName(CSettings::getInstance()->getSetting("DatabaseServer"));
	setDatabaseName(CSettings::getInstance()->getSetting("Database"));
	setUserName(CSettings::getInstance()->getSetting("DatabaseUserName"));
	setPassword(CSettings::getInstance()->getSetting("DatabasePassword"));
}

CCARPCDBConnection::~CCARPCDBConnection() {
	// TODO Auto-generated destructor stub
}
