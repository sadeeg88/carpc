/*
 * cp_SystemControl.h
 *
 *  Created on: 18.04.2011
 *      Author: Sascha
 */

#ifndef CP_SYSTEMCONTROL_H_
#define CP_SYSTEMCONTROL_H_

#include <qobject.h>
#include <qsocketnotifier.h>

#ifdef Q_WS_X11
int setup_unix_signal_handlers();
#endif


class cp_SystemControl : public QObject
{
  Q_OBJECT
private:
  static cp_SystemControl * instance;
  cp_SystemControl(QObject *parent = 0, char * name = 0);
  virtual
  ~cp_SystemControl();
public:
  static cp_SystemControl * getInstance();
  static void Destroy();
  signals:
   void SystemTerm();

#ifdef Q_WS_X11
   // Unix signal handlers.
 public:
    static void hupSignalHandler(int unused);
    static void termSignalHandler(int unused);

  public slots:
    // Qt signal handlers.
    void handleSigHup();
    void handleSigTerm();

  private:
   static int sighupFd[2];
   static int sigtermFd[2];

    QSocketNotifier *snHup;
    QSocketNotifier *snTerm;
#endif


};

#endif /* CP_SYSTEMCONTROL_H_ */
