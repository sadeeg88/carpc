/*
 * CSettings.cpp
 *
 *  Created on: 15.11.2009
 *      Author: Sascha
 */
#include <qstring.h>
#include <qfile.h>
#include "CSettings.h"

  CSettings* CSettings::instance = NULL;

  CSettings* CSettings::getInstance()
  {
    if( !instance )
      {
      instance = new CSettings();
      instance->ReadConfigFile();
      }

    return instance;
  }

  void CSettings::Destroy()
  {
    // static
    if (instance){
      delete instance;
      instance = NULL;
    }
  }


void CSettings::ReadConfigFile()
{
    QFile file("carpc.config");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
         return;

     while (!file.atEnd()) {
         QByteArray line = file.readLine();
         cp_SettingsObjekt* Setting;
         QString strLine(line);
         int i_Pos = strLine.indexOf(';');
         if(i_Pos > 0)
           {
            QString strName,strValue;
            Setting = new cp_SettingsObjekt();
            strName= strLine.left(i_Pos);
            std::string lol = strName.toStdString();
            Setting->m_strName = strName;
            strValue = strLine.mid(i_Pos+1);
            strValue.remove('\n');
            strValue.remove('\r');
            Setting->m_strValue = strValue;
            std::string lol2 = strValue.toStdString();
            listObjekt.append(Setting);
           }
         //process_line(line);
     }
}

void CSettings::Reload()
{

}


QString CSettings::getSetting(QString Key)
{

  for(int x = 0 ; x < listObjekt.count(); x++)
  {
    if(listObjekt.at(x)->m_strName.compare(Key) == 0)
    {
      return listObjekt.at(x)->m_strValue;
    }

  }
	return "";
}
