/*
 * CSettings.h
 *
 *  Created on: 15.11.2009
 *      Author: Sascha
 */

#ifndef CSETTINGS_H_
#define CSETTINGS_H_
#include <qlist.h>

class cp_SettingsObjekt
{
  public:
    QString m_strName;
    QString m_strValue;

};

class CSettings {
private:
	static CSettings * instance;
	CSettings(){};
	virtual ~CSettings(){};
	void ReadConfigFile();
public:
   static CSettings * getInstance();
   static void Destroy();
   void Reload();
   QString getSetting(QString Key);
   QList <cp_SettingsObjekt*> listObjekt;

};

#endif /* CSETTINGS_H_ */
