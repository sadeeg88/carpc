/*
 * CLog.cpp
 *
 *  Created on: 15.11.2009
 *      Author: Sascha
 */
#include <qstring.h>
#include <qdatetime.h>
#include <qfile.h>
#include "CLog.h"
#include "CSettings.h"


 CLog* CLog::instance = NULL;

 CLog* CLog::getInstance()
  {
    if( !instance )
      instance = new CLog();
    return instance;
  }

  void CLog::Destroy()
  {
    // static
    if (instance){
      delete instance;
      instance = NULL;
    }
  }


CLog::CLog() {


}

CLog::~CLog() {
	// TODO Auto-generated destructor stub
}

void CLog::Log(QString Message,int LogLevel = LOG_ERRORS)
{
	bool bSystemLogOK;
	int SystemLogLevel = CSettings::getInstance()->getSetting("LogLevel").toInt(&bSystemLogOK,10);
	if(bSystemLogOK==false)
	{
		SystemLogLevel = 1;
	}
	QTime time = QTime::currentTime();
	QDate date = QDate::currentDate();

	if(SystemLogLevel >= LogLevel)
	{
		QFile SchreibZugriff;
		SchreibZugriff.setFileName(CSettings::getInstance()->getSetting("LogFile"));
		if(SchreibZugriff.open(QIODevice::Append))
		{
			QString LogMessage = date.toString("dd.MM.yyyy")+" "+ time.toString("HH:mm:ss.zzz")+" <> ";
			LogMessage += Message+"\r\n";
			SchreibZugriff.write(LogMessage.toStdString().c_str());
			SchreibZugriff.close();
		}

	}
}
