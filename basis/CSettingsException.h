/*
 * CSettingsException.h
 *
 *  Created on: 15.11.2009
 *      Author: Sascha
 */

#include "CCARPCExcetpion.h"
#ifndef CSETTINGSEXCEPTION_H_
#define CSETTINGSEXCEPTION_H_

class CSettingsException  : public CCARPCExcetpion
 {
 public:
	 CSettingsException(QString strMessage): CCARPCExcetpion(strMessage){};
     void raise() const { throw *this; }
     Exception *clone() const { return new CCARPCExcetpion(*this); }
     QString getExceptionType() const { return QString("CSettingsException");}
 };

#endif /* CSETTINGSEXCEPTION_H_ */
