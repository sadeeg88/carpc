/*
 * cp_Application.h
 *
 *  Created on: 25.04.2011
 *      Author: Sascha
 */
#include <QtGui>
#include <QApplication>

#ifndef CP_APPLICATION_H_
#define CP_APPLICATION_H_

class cp_Application : public QApplication
{
    Q_OBJECT
public:
  cp_Application(int &argc, char *argv[]);
  void saveState(QSessionManager &sessionManager);

  virtual  ~cp_Application();
};

#endif /* CP_APPLICATION_H_ */
