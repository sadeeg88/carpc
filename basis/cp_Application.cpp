/*
 * cp_Application.cpp
 *
 *  Created on: 25.04.2011
 *      Author: Sascha
 */

#include "cp_Application.h"
#include <qstringlist.h>
#include "basis/CLog.h"
#include "basis/CCARPCDBConnection.h"
#include <qfile.h>
#include "multimedia/cp_MediaPlayer.h"
#include "file/CCARPCFileFinder.h"
#include "multimedia/cp_MediaBibliothek.h"
#include "gui/ccarpctouchkeyboard_context.h"
#include "gui/cp_dlgMainMenu.h"
#include "ui_test.h"
#include "gui/ccarpcstyle.h"
#include "basis/cp_SystemControl.h"

cp_Application::cp_Application(int &argc, char *argv[])
:QApplication(argc, argv)
{
  // TODO Auto-generated constructor stub

}

cp_Application::~cp_Application()
{
  // TODO Auto-generated destructor stub
}

void cp_Application::saveState(QSessionManager &sessionManager)
{
    cp_MediaPlayer::getInstance()->SaveCurrentPlay();
    QStringList discardCommand;
    //discardCommand << "rm" << fileName;
    sessionManager.setDiscardCommand(discardCommand);
}


