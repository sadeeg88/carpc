/*
 * CCARPCExcetpion.h
 *
 *  Created on: 15.11.2009
 *      Author: Sascha
 */


#ifndef CCARPCEXCETPION_H_
#define CCARPCEXCETPION_H_

class CCARPCExcetpion : public QtConcurrent::Exception
 {
 private:
     QString m_strExceptionMessage;
 public:
     void raise() const { throw *this; }
     Exception *clone() const { return new CCARPCExcetpion(*this); }
     CCARPCExcetpion(QString strMessage);
     ~CCARPCExcetpion() throw();
     QString getExceptionType() const { return QString("CCARPCExcetpion");}
     QString getExceptionMessage();

 };

#endif /* CCARPCEXCETPION_H_ */
