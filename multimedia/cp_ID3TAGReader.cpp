/*
 * cp_ID3TAGReader.cpp
 *
 *  Created on: 09.01.2011
 *      Author: Sascha
 */

#include "cp_ID3TAGReader.h"
#include "../file/CCARPCFileFinder.h"
#include "../basis/CLog.h"
#include "../basis/CSettings.h"
#include "../multimedia/cp_ID3ENCODE.h"

cp_ID3TAGReader::cp_ID3TAGReader()
{
  // TODO Auto-generated constructor stub

}

cp_ID3TAGReader::~cp_ID3TAGReader()
{
  // TODO Auto-generated destructor stub
}

cp_ID3Object cp_ID3TAGReader::ReadID3Tag(QString strFilename)
{

       cp_ID3Object cpID3TAG;


       CLog::getInstance()->Log("Ermittle ID TagInfo:" + strFilename,LOG_DEBUGDEVELOP);

         ID3_Tag tempID;
         tempID.Link(strFilename.toStdString().c_str(), ID3TT_ALL);
         ID3_Frame* myFrame = NULL;
         myFrame = tempID.Find(ID3FID_TITLE);
         if (NULL != myFrame)
         {

           ID3_Field * field  = myFrame->GetField(ID3FN_TEXT);
           if(field != NULL)
           {
             cpID3TAG.Title = ID3FieldToQString(field);
           }
         }
         if(cpID3TAG.Title.isEmpty())
         {
           cpID3TAG.Title="Unbekannt";
         }




          myFrame = tempID.Find(ID3FID_LEADARTIST);
          if (NULL != myFrame)
          {
              ID3_Field * field  = myFrame->GetField(ID3FN_TEXT);
              if(field != NULL)
              {
                cpID3TAG.Interpret = ID3FieldToQString(field);
              }

          }
          if(cpID3TAG.Interpret.isEmpty())
          {
            cpID3TAG.Interpret="Unbekannt";
          }


          myFrame = tempID.Find(ID3FID_ALBUM);
          if (NULL != myFrame)
          {
              ID3_Field * field  = myFrame->GetField(ID3FN_TEXT);
              if(field != NULL)
              {
                cpID3TAG.Album = ID3FieldToQString(field);
              }
          }
          if(cpID3TAG.Album.isEmpty())
          {
            cpID3TAG.Album="Unbekannt";
          }


          myFrame = tempID.Find(ID3FID_TRACKNUM);
          if (NULL != myFrame)
          {
             const char* Tracknr = myFrame->GetField(ID3FN_TEXT)->GetRawText();
             bool bOK;
             cpID3TAG.TrackNr=QString(Tracknr).toInt(&bOK,10);
          }
          else
          {
              cpID3TAG.TrackNr=99;
          }

          cpID3TAG.Source = strFilename;

          return cpID3TAG;


}
