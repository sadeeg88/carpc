/**************************************************************************
 * @brief       cp_MediaPlayer
 * @autor       Sascha Deeg
 * @date        27.11.2009
 *
 * Die Klasse cp_MediaPlayer stellt die
 * folgenden Funktionen zu Verfügung
 * - Wiedergabe von Musikfiles
 * - Steuern der Lautstärke
 * Die Klasse cp_MediaPlayer wurde als Singelton implementiert.
 ************************************************************************* */


#include "../multimedia/cp_MediaPlayer.h"
#include "../multimedia/cp_MediaBibliothek.h"
#include "../basis/cp_SystemControl.h"
#include "../basis/CLog.h"
#include <qmessagebox.h>
#include <objectdescription.h>

cp_MediaPlayer* cp_MediaPlayer::instance = NULL;

cp_MediaPlayer::cp_MediaPlayer()
{
  m_pMediaObjekt = new Phonon::MediaObject();
  connect(m_pMediaObjekt,SIGNAL(stateChanged(Phonon::State,Phonon::State)),this,SLOT(StateChange(Phonon::State,Phonon::State)));
  m_pMediaController = new Phonon::MediaController(m_pMediaObjekt);
  m_pAudioOutput = new Phonon::AudioOutput(Phonon::VideoCategory);
  Phonon::createPath(m_pMediaObjekt, m_pAudioOutput);
  m_bMuted = false;
  m_repeatMode = repeat_none;
  currentMode = MP3;
  setVolume(10);
  LoadCurrentPlay();
  connect(cp_SystemControl::getInstance(),SIGNAL(SystemTerm()),this,SLOT(SaveCurrentPlay()));
}

void cp_MediaPlayer::clear()
{
  m_pMediaObjekt->stop();
  m_pMediaObjekt->clearQueue();
  m_pMediaObjekt->clear();
  m_ListPlayList.clear();
  m_iCurrentTitle = 0;
  currentMode = MP3;
}

cp_MediaPlayer::~cp_MediaPlayer()
{
  SaveCurrentPlay();
  delete m_pMediaObjekt;
  delete m_pMediaController;
  m_pMediaObjekt = NULL;
  m_pMediaController = NULL;
}

cp_MediaPlayer*
cp_MediaPlayer::getInstance()
{
  if (!instance)
    instance = new cp_MediaPlayer();
  return instance;
}

void
cp_MediaPlayer::Destroy()
{
  // static
  if (instance)
    {
      delete instance;
      instance = NULL;
    }
}

void cp_MediaPlayer::setPlayList(QStringList PlayList, int iTitle = 0,cp_MediaPlayerMode mode = MP3)
{
  currentMode = mode;
  m_pMediaObjekt->disconnect(m_pMediaObjekt, SIGNAL(finished ()), this,
      SLOT(nextSong()));

  m_pMediaObjekt->stop();
  m_pMediaObjekt->clear();
  m_ListPlayList = PlayList;
  m_iCurrentTitle = iTitle;
  int CVol = getVolume();

  if(currentMode == MP3 ||currentMode == Video)
    {
  if (PlayList.count() > iTitle)
    {
      CLog::getInstance()->Log(PlayList.at(iTitle), LOG_ERRORS);
      m_pMediaObjekt->setCurrentSource(Phonon::MediaSource(PlayList.at(iTitle)));
      m_pMediaObjekt->connect(m_pMediaObjekt, SIGNAL(finished ()), this,
          SLOT(nextSong()));
      m_pMediaObjekt->play();
      QList<Phonon::ObjectDescription<(Phonon::ObjectDescriptionType)2u> > list = m_pMediaController->availableAudioChannels();
      QMessageBox msg;
    }
    }
  else if(currentMode == CD)
    {
        m_pMediaObjekt->setCurrentSource(Phonon::Cd);
        m_pMediaController->setCurrentTitle(1);
        m_pMediaObjekt->play();
    }
  else if(currentMode == DVD)
    {
        m_pMediaObjekt->setCurrentSource(Phonon::MediaSource(Phonon::Dvd));
        //m_pMediaController->
        //m_pMediaObjekt->play();
    }
  setVolume(CVol);

}
void
cp_MediaPlayer::Play()
{
  if (m_pMediaObjekt->state() == Phonon::PlayingState)
    m_pMediaObjekt->pause();
  else if (m_pMediaObjekt->state() == Phonon::PausedState)
    m_pMediaObjekt->play();
}

void
cp_MediaPlayer::SkipForward()
{
  if (currentMode == MP3)
    {
      if (!((m_iCurrentTitle + 1) > m_ListPlayList.count() - 1))
        {
          m_pMediaObjekt->stop();
          m_pMediaObjekt->clearQueue();
          m_pMediaObjekt->setCurrentSource(Phonon::MediaSource(
              m_ListPlayList.at(m_iCurrentTitle + 1)));
          m_iCurrentTitle++;
          m_pMediaObjekt->play();
        }
    }
  else if (currentMode == CD)
  {
    //Prüft, ob nächster Titel innerhalb der möglichen Titel liegt
    if(m_pMediaController->availableTitles()> m_pMediaController->currentTitle())
      {
        m_pMediaController->nextTitle();
      }
  }
}
void
cp_MediaPlayer::SkipBack()
{
  if (currentMode == MP3)
    {
      if (!((m_iCurrentTitle - 1) < 0))
        {
          m_pMediaObjekt->stop();
          m_pMediaObjekt->clearQueue();
          m_pMediaObjekt->setCurrentSource(Phonon::MediaSource(
              m_ListPlayList.at(m_iCurrentTitle - 1)));
          m_iCurrentTitle--;
          m_pMediaObjekt->play();
        }
    }
  else if (currentMode == CD)
    {
    m_pMediaController->previousTitle();
    }
}
void
cp_MediaPlayer::SkipToTitle(int TitleNr)
{
  if (currentMode == MP3)
    {
      m_pMediaObjekt->stop();
      m_pMediaObjekt->clear();
      m_iCurrentTitle = TitleNr;
      if (m_ListPlayList.count() > TitleNr)
        {
          m_pMediaObjekt->setCurrentSource(Phonon::MediaSource(
              m_ListPlayList.at(TitleNr)));
          m_pMediaObjekt->connect(m_pMediaObjekt, SIGNAL(finished ()), this,
              SLOT(nextSong()));
        }
      m_pMediaObjekt->play();
    }
  else if (currentMode == CD)
    {
    m_pMediaController->setCurrentTitle(TitleNr);
    }
}

void
cp_MediaPlayer::VolumeUp(int iProzent)
{
  CLog::getInstance()->Log(QString("cp_MediaPlayer::VolumeUp"), LOG_INFOS);
  qreal dNewVolume = m_pAudioOutput->volume();
  int iVol = qRound(dNewVolume * 100);
  if (iVol >= 95)
    {
      iVol = 100;
    }
  else
    {
      iVol = iVol + iProzent;
    }
  QString LogMessage;
  LogMessage = "WertAlt";
  LogMessage += QString::number(iVol);
  CLog::getInstance()->Log(LogMessage, LOG_INFOS);

  setVolume(iVol);
}

void
cp_MediaPlayer::VolumeDown(int iProzent)
{
  CLog::getInstance()->Log(QString("cp_MediaPlayer::VolumeDown"), LOG_INFOS);
  qreal dNewVolume = m_pAudioOutput->volume();
  int iVol = qRound(dNewVolume * 100);
  if (iVol <= 5)
    {
      iVol = 0;
    }
  else
    {
      iVol = iVol - iProzent;
    }
  QString LogMessage;
  LogMessage = "WertAlt";
  LogMessage += QString::number(iVol);
  CLog::getInstance()->Log(LogMessage, LOG_INFOS);

  setVolume(iVol);
}

void
cp_MediaPlayer::setVolume(int iProzent)
{
  if (iProzent >= 0 && iProzent <= 100)
    {
      m_pAudioOutput->setVolume(1.0 / 100.0 * iProzent);
    }

}

int
cp_MediaPlayer::getVolume()
{
  return (int) (qRound((m_pAudioOutput->volume()) * 100));
}

bool
cp_MediaPlayer::getMuted()
{
  return m_bMuted;
}

void
cp_MediaPlayer::Muted(bool bMuted)
{
  m_bMuted = bMuted;
  m_pAudioOutput->setMuted(bMuted);
}

void
cp_MediaPlayer::nextSong()
{
  m_pMediaObjekt->disconnect(m_pMediaObjekt, SIGNAL(finished ()), this,
      SLOT(nextSong()));
  if (currentMode == MP3)
    {
      m_pMediaObjekt->stop();
      m_pMediaObjekt->clearQueue();
      switch (m_repeatMode)
        {
      case repeat_none:
        if (!((m_iCurrentTitle + 1) > m_ListPlayList.count() - 1))
          {
            m_pMediaObjekt->setCurrentSource(Phonon::MediaSource(
                m_ListPlayList.at(m_iCurrentTitle + 1)));
            m_iCurrentTitle++;
          }

        break;
      case repeat_title:
        m_pMediaObjekt->setCurrentSource(Phonon::MediaSource(m_ListPlayList.at(
            m_iCurrentTitle)));

        break;
      case repeat_all:
        if (!((m_iCurrentTitle + 1) > m_ListPlayList.count() - 1))
          {
            m_pMediaObjekt->setCurrentSource(Phonon::MediaSource(
                m_ListPlayList.at(m_iCurrentTitle + 1)));
            m_iCurrentTitle++;
          }
        else
          {
            m_pMediaObjekt->setCurrentSource(Phonon::MediaSource(
                m_ListPlayList.at(0)));
            m_iCurrentTitle = 0;
          }
        }
      m_pMediaObjekt->play();
      CLog::getInstance()->Log(QString("SkiptoTitle") + m_ListPlayList.at(
          m_iCurrentTitle), LOG_INFOS);
      m_pMediaObjekt->connect(m_pMediaObjekt, SIGNAL(finished ()), this,
          SLOT(nextSong()));
    }
  else if (currentMode == CD)
    {

    m_pMediaController->nextTitle();
    }

}

QString
cp_MediaPlayer::getArtistTitleasQString()
{

  QString ret;
  if (currentMode == MP3)
    {
      const cp_ID3Object * p_MMO =
          cp_MediaBibliothek::getInstance()->getTitleInformation(
              m_pMediaObjekt->currentSource().fileName());
      if (p_MMO != NULL)
        {
          ret += p_MMO->Interpret;
          ret += " - ";
          ret += p_MMO->Title;
        }
    }
   else if(currentMode == CD)
    {
      ret += "CD";
      ret += " - ";
      ret += "Title"+QString::number(m_pMediaController->currentTitle(),10);
    }

  return ret;
}

void cp_MediaPlayer::StateChange(Phonon::State newstate, Phonon::State oldstate)
{
  if(newstate == Phonon::ErrorState)
  {
    CLog::getInstance()->Log(QString("Phonon Fehler aufgetreten: "+m_pMediaObjekt->errorString()), LOG_INFOS);
  }
  else if(oldstate == Phonon::ErrorState)
  {
    CLog::getInstance()->Log(QString("Phonon hat jetzt keinen Fehler mehr. "), LOG_INFOS);
  }
}

qreal  cp_MediaPlayer::getVolumeinDezibel()
{
  return m_pAudioOutput->volumeDecibel();
}

void cp_MediaPlayer::SaveCurrentPlay()
{

  int play;
  if (m_pMediaObjekt->state() == Phonon::PlayingState)
      play = 1;
    else if (m_pMediaObjekt->state() == Phonon::PausedState)
      play = 0;
  int l = m_ListPlayList.count();
  if (m_ListPlayList.count() > 0)
  {
    QFile file("LastPlay.save");
       if (!file.open(QIODevice::ReadWrite | QIODevice::Truncate| QIODevice::Text))
            return ;

      for(int x =0; x < m_ListPlayList.count(); x++)
      {
        file.write(m_ListPlayList.at(x).toAscii());
        file.write(";\r\n");
      }
      file.close();
  }

  QFile file2("MPConf.save");
        if (!file2.open(QIODevice::ReadWrite | QIODevice::Truncate| QIODevice::Text))
             return ;
  file2.write(QString::number(m_iCurrentTitle,10).toAscii());
  file2.write(";");
  file2.write(QString::number(currentMode,10).toAscii());
  file2.write(";");
  file2.write(QString::number(m_bMuted,10).toAscii());
  file2.write(";");
  file2.write(QString::number(m_repeatMode,10).toAscii());
  file2.write(";");
  file2.write(QString::number(play,10).toAscii());
  file2.write(";\r\n");

  file2.close();


}

void cp_MediaPlayer::LoadCurrentPlay()
{
  QStringList tmpListPlayList;
  int tmpCurrentTitle = 0;
  cp_MediaPlayerMode tmpCurrentMode = MP3;
  bool tmpbMuted = false;
  int Play;
  CCARPCMediaRM tmpRepeatmode = repeat_none;



  QFile file("LastPlay.save");
  if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {

       while (!file.atEnd()) {

         QByteArray line = file.readLine();
           QString strLine(line);
           int i_Pos = strLine.indexOf(';',0);
           if(i_Pos > 0)
           {
             tmpListPlayList.append(strLine.mid(0,(i_Pos)));
           }
       }
       file.close();
    }

  QFile file2("MPConf.save");
  if (!file2.open(QIODevice::ReadOnly | QIODevice::Text))
    return;

  QByteArray line2 = file2.readLine();
  QString strLine2(line2);

  QString values[5];
  int i_stPos = 0;
  bool bOK = true;

  for (int x = 0; x < 5; x++)
    {
      int i_Pos = strLine2.indexOf(';', i_stPos);
      if (i_Pos > 0)
        {
          values[x] = strLine2.mid(i_stPos, (i_Pos - i_stPos));
          i_stPos = i_Pos + 1;

        }
      else
        {
          bOK = false;
          break;
        }
    }
 file2.close();

  if (bOK)
    {
      bool tmpdummy;
      tmpCurrentTitle = values[0].toInt(&tmpdummy,10);
      tmpCurrentMode = (cp_MediaPlayerMode) values[1].toInt(&tmpdummy,10);
      tmpbMuted = (bool) values[2].toInt(&tmpdummy,10);
      tmpRepeatmode = (CCARPCMediaRM) values[3].toInt(&tmpdummy,10);
      Play = values[4].toInt(&tmpdummy,10);
      setPlayList(tmpListPlayList,tmpCurrentTitle,tmpCurrentMode);
      if(Play == 1)
        m_pMediaObjekt->play();
      else
        m_pMediaObjekt->pause();

    }


}