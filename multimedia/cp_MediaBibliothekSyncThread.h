/*
 * cp_MediaBibliothekSyncThread.h
 *
 *  Created on: 09.01.2011
 *      Author: Sascha
 */

#ifndef CP_MEDIABIBLIOTHEKSYNCTHREAD_H_
#define CP_MEDIABIBLIOTHEKSYNCTHREAD_H_

class cp_MediaBibliothekSyncThread
{
public:
  cp_MediaBibliothekSyncThread();
  void run();
  virtual
  ~cp_MediaBibliothekSyncThread();

 // signals:
  //void refreshMediaBibliothek( int howMany );

};

#endif /* CP_MEDIABIBLIOTHEKSYNCTHREAD_H_ */
