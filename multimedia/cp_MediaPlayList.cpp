/*
 * cp_MediaPlayList.cpp
 *
 *  Created on: 15.06.2011
 *      Author: Sascha
 */

#include "cp_MediaPlayList.h"
#include <qfile.h>
#include <QtXml/qdom.h>
#include <QXmlParseException>
#include <qtextstream.h>


cp_MediaPlayList::cp_MediaPlayList(QString Filename) {
	// TODO Auto-generated constructor stub
	m_strFilename = Filename;
	LoadPlayList();

}
cp_MediaPlayList::cp_MediaPlayList(QString Filename,QString strName)
{
	m_strFilename = Filename;
	LoadPlayList();
	m_strName = strName;
}
cp_MediaPlayList::cp_MediaPlayList(QString Filename,QString strName, QStringList Tracks){
	m_strFilename = Filename;
	LoadPlayList();
	m_strName = strName;
	m_Tracks.append(Tracks);

}
bool cp_MediaPlayList::existsPlayListFile()
{
	return m_exists;
}

bool cp_MediaPlayList::LoadPlayList()
{
	QFile file(m_strFilename);
	if(file.exists())
	{
		m_exists = true;
		 QDomDocument doc("CarPC");
		 if (!file.open(QIODevice::ReadOnly))
		     return false;
		 if (!doc.setContent(&file)) {
		     file.close();
		     return false;

		 }
		 file.close();

		 QDomNode root = doc.firstChild();
		 if(!root.isNull())
		 {
		 QDomNode e = root.firstChild();
		 while(!e.isNull()) {
		      QDomElement d = e.toElement(); // try to convert the node to an element.
		      if(!d.isNull()) {
		         if(d.nodeName().compare("Name") == 0)
		         {
		        	 m_strName = d.firstChild().nodeValue();
		         }
		         else if(d.nodeName().compare("Tracks") == 0)
		         {
		        	 QDomNode Tr = d.firstChild();
		        	 while(!Tr.isNull())
		        	 {
		        		 QDomElement TrE = Tr.toElement();
		        		 if(!TrE.isNull())
		        		 {
		        			 if(TrE.nodeName().compare("src") == 0)
		        			 {
		        				m_Tracks.append( TrE.firstChild().nodeValue());

		        			 }
		        		 }
		        		 Tr = Tr.nextSibling();
		        	 }


		         }
		      }
		      e = e.nextSibling();
		  }


		 }


	}
	else
	{
		m_exists = false;
		return false;
	}
	return true;
}

cp_MediaPlayList::~cp_MediaPlayList() {
	// TODO Auto-generated destructor stub
}
QString cp_MediaPlayList::getName()
{
 return m_strName;
}

void cp_MediaPlayList::setName(QString strName)
{
	m_strName = strName;
}

void cp_MediaPlayList::addTrack(QString strPath)
{
	if(!m_Tracks.contains(strPath,Qt::CaseSensitive))
	{
		m_Tracks.append(strPath);
	}
}

void cp_MediaPlayList::deleteTrack(QString strPath)
{
	for(int x = 0; x < m_Tracks.count(); x++)
	{
		if(m_Tracks.at(x).compare(strPath) == 0)
		{
			m_Tracks.removeAt(x);
			return;
		}
	}
}

QString cp_MediaPlayList::getTrack(int iNumber)
{
	return m_Tracks.at(iNumber);
}

QStringList cp_MediaPlayList::getAllTracks()
{
	return m_Tracks;
}

bool cp_MediaPlayList::SavePlayList()
{

	return SavePlayListAS(m_strFilename);

}

bool cp_MediaPlayList::SavePlayListAS(QString strFilename)
{
    QDomDocument doc("CarPC");
    QDomElement root = doc.createElement("PlayList");
    doc.appendChild(root);

    QDomElement Name = doc.createElement("Name");
    root.appendChild(Name);

    QDomText NameInhalt = doc.createTextNode(m_strName);
    Name.appendChild(NameInhalt);

    QDomElement Tracks = doc.createElement("Tracks");
    root.appendChild(Tracks);

    for(int x=0; x < m_Tracks.count();x++)
    {
        QDomElement Track =  doc.createElement("src");
        Tracks.appendChild(Track);

        QDomText TrackInhalt  = doc.createTextNode(m_Tracks.at(x));
        Track.appendChild(TrackInhalt);
    }
    QString xml = doc.toString();


    QFile file(strFilename);
    if(file.open(QIODevice::WriteOnly|QIODevice::Truncate|QIODevice::Text))
    {
    	QTextStream text(&file);
    	doc.save(text,0);
    	//file.write(xml.toAscii());
    	file.close();
    }
    else
    	return false;
    return true;

}

bool cp_MediaPlayList::isUseable()
{
	if((!m_strName.isEmpty())&&(!m_strName.isNull())&&m_Tracks.count()>0)
		return true;
	return false;
}

void cp_MediaPlayList::changePos(int iOldPos,int iNewPos)
{
	m_Tracks.move(iOldPos,iNewPos);
}
void cp_MediaPlayList::upPos(int iOldPos)
{
	m_Tracks.move(iOldPos,iOldPos-1);
}
void cp_MediaPlayList::downPos(int iOldPos){
	m_Tracks.move(iOldPos,iOldPos+1);
}
