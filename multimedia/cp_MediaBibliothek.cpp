/**************************************************************************
 * @brief       cp_MediaBibliothek
 * @autor       Sascha Deeg
 * @date        28.10.2010
 *
 * Die Klasse cp_MediaBibliothek stellt die
 * folgenden Funktionen zu Verf�gung
 * - Ermitteln der Musikdateien in einem Verzeichniss
 * - Ermitteln der ID3 Tags der Musikdateien
 * - Stellt die notwendigen Funktionen eine Musikbibliotek zur Verf�gung
 * Die Klasse cp_MediaBibliothek wurde als Singelton implementiert.
 ************************************************************************* */

#include "cp_MediaBibliothek.h"
#include <id3/tag.h>
#include <iostream>
#include <qdatetime.h>

cp_MediaBibliothek* cp_MediaBibliothek::instance = NULL;

cp_MediaBibliothek::cp_MediaBibliothek()
{


}

cp_MediaBibliothek::~cp_MediaBibliothek()
{
  // TODO Auto-generated destructor stub
}

/**************************************************************************
 * @brief       Gibt die Instanz der cp_MediaBibliothek zur�ck
 * @autor       Sascha Deeg
 * @date        28.10.2010
 * @return      Instanz der cp_MediaBibliothek
 ************************************************************************* */
cp_MediaBibliothek* cp_MediaBibliothek::getInstance()
{
  if (!instance)
    instance = new cp_MediaBibliothek();
  return instance;
}

/**************************************************************************
 * @brief       Zerst�rt die Instanz der cp_MediaBibliothek
 * @autor       Sascha Deeg
 * @date        28.10.2010
 ************************************************************************* */
void cp_MediaBibliothek::Destroy()
{
  // static
  if (instance)
    {
      delete instance;
      instance = NULL;
    }
}

/**************************************************************************
 * @brief       L�d die File in die Mediabibiliothek
 * @autor       Sascha Deeg
 * @date        28.10.2010
 ************************************************************************* */
void cp_MediaBibliothek::LoadMoviesinMediaBibliothek()
{
  CCARPCFileFinder MusikSourceFinder(CSettings::getInstance()->getSetting("MovieDir"));
  QFileInfoList MediaSourceList = MusikSourceFinder.findFiles("*.mp4 *.avi *.mpeg *.mpg");
  for (int x = 0; x < MediaSourceList.count(); x++)
    {
      QFileInfo tempFile = MediaSourceList.at(x);
      cp_ID3Object tmpOID3Object;
      tmpOID3Object.Source = tempFile.filePath();
      tmpOID3Object.Title = tempFile.fileName();
      m_MovieBiblothek.append(tmpOID3Object);
    }

}

/**************************************************************************
 * @brief       L�d die Musikfiles in die Mediabibiliothek
 * @autor       Sascha Deeg
 * @date        28.10.2010
 ************************************************************************* */
void cp_MediaBibliothek::LoadMusikFileinMediaBibliothek()
{
  m_MusikBiblothek.clear();
  CLog::getInstance()->Log("void cp_MediaBibliothek::LoadMusikFileinMediaBibliothek()",LOG_DEBUGDEVELOP);
  CCARPCFileFinder MusikSourceFinder(CSettings::getInstance()->getSetting("MusikDir"));
  QTime time = QTime::currentTime();
  QFileInfoList MediaSourceList = MusikSourceFinder.findFiles("*.mp3 *.ogg");
  CLog::getInstance()->Log("Ermittlung der MP3 Files: (in SEC)"+QString::number(time.secsTo(QTime::currentTime()),10), LOG_INFOS);
  QFile ErrorFile;
  ErrorFile.setFileName("ErrorMusik.txt");
  bool bEr = ErrorFile.open(QIODevice::Truncate | QIODevice::ReadWrite);
  CLog::getInstance()->Log("Ermittelte Musikfiles:" + QString().number(
      MediaSourceList.count(), 10), LOG_INFOS);
  QTime Tagtime = QTime::currentTime();
  for (int x = 0; x < MediaSourceList.count(); x++)
    {
      QFileInfo tempFile = MediaSourceList.at(x);
      cp_ID3TAGReader cID3Reader;
      m_MusikBiblothek.append(cID3Reader.ReadID3Tag( tempFile.filePath()));
    }


  if (bEr)
    {
      ErrorFile.close();
    }

  CLog::getInstance()->Log("Lesedauer der Tag (in SEC)"+QString::number(Tagtime.secsTo(QTime::currentTime()),10), LOG_INFOS);
  CLog::getInstance()->Log("Ladedauer der Mediabibliothek (in SEC)"+QString::number(time.secsTo(QTime::currentTime()),10), LOG_INFOS);
  GenerateMediaBiblothekFile();


}

void cp_MediaBibliothek::LoadPlayListsinMediaBibliothek()
{
	  CCARPCFileFinder MusikSourceFinder(CSettings::getInstance()->getSetting("MusikDir"));
	  QFileInfoList MediaSourceList = MusikSourceFinder.findFiles("*.carpcPL");
	  for (int x = 0; x < MediaSourceList.count(); x++)
	  {
	      cp_MediaPlayList Playlist(MediaSourceList.at(x).filePath());
	      if(Playlist.isUseable())
	      {
	    	  m_Playlists.append(Playlist);
	      }
	   }
}

QStringList cp_MediaBibliothek::getPlayLists()
{
	QStringList Playlists;
	for(int x=0; x < m_Playlists.count();x++)
	{
		cp_MediaPlayList PL = m_Playlists.at(x);
		Playlists.append(PL.getName());
	}
	return Playlists;

}

QStringList cp_MediaBibliothek::getPlaylistTitles(QString strPlaylist)
{
	QStringList Titels;
	for(int x=0; x < m_Playlists.count();x++)
	{
		cp_MediaPlayList PL = m_Playlists.at(x);
		if(PL.getName().compare(strPlaylist) == 0)
		{
			QStringList sources = PL.getAllTracks();
			for(int y = 0; y < sources.count(); y++)
			{
				const cp_ID3Object * aktMP3 = getTitleInformation(sources.at(y));
				if(aktMP3 != NULL )
				{
					QString View;
					View += aktMP3->Interpret;
					View += " - ";
					View += aktMP3->Title;
					Titels.append(View);
				}
			}
		}
	}
	return Titels;

}

QStringList cp_MediaBibliothek::getPlaylistTitlesSource(QString strPlaylist)
{
	QStringList Sources;
	for(int x=0; x < m_Playlists.count();x++)
	{
		cp_MediaPlayList PL = m_Playlists.at(x);
		if(PL.getName().compare(strPlaylist) == 0)
		{
			QStringList sources = PL.getAllTracks();
			for(int y = 0; y < sources.count(); y++)
			{
				const cp_ID3Object * aktMP3 = getTitleInformation(sources.at(y));
				if(aktMP3 != NULL )
				{

					Sources.append(sources.at(y));
				}
			}
		}
	}
	return Sources;

}


/**************************************************************************
 * @brief       Ermittelt alle Alben in der MediaBibliothek
 * @autor       Sascha Deeg
 * @date        28.10.2010
 * @return      Gibt alle Alben aus der Mediabibliothek als Liste zur�ck
 ************************************************************************* */
QStringList cp_MediaBibliothek::getAllAlbum()
{
  QStringList Album;
  for (int x = 0; x < m_MusikBiblothek.count(); x++)
    {
      Album.append(m_MusikBiblothek.at(x).Album);
    }
  Album.sort();
  Album.removeDuplicates();
  Album.insert(0,"[All]");
  return Album;
}

/**************************************************************************
 * @brief       Ermittelt alle Interpreten in der MediaBibliothek
 * @autor       Sascha Deeg
 * @date        28.10.2010
 * @return      Gibt alle Interpreten aus der Mediabibliothek als Liste zur�ck
 ************************************************************************* */
QStringList cp_MediaBibliothek::getAllInterpreten()
{
  QStringList Interpreten;
  for (int x = 0; x < m_MusikBiblothek.count(); x++)
    {
      Interpreten.append(m_MusikBiblothek.at(x).Interpret);
    }
  Interpreten.sort();
  Interpreten.removeDuplicates();
  Interpreten.insert(0,"[All]");
  return Interpreten;
}

/**************************************************************************
 * @brief       Ermittelt Alben eines Interpreten in der MediaBibliothek
 * @autor       Sascha Deeg
 * @date        28.10.2010
 * @param       Interpret Namen des Interpret f�r den die Alben ermittelt
 *              werden sollen.
 * @return      Gibt die Alben aus der Mediabibliothek als Liste zur�ck
 ************************************************************************* */
QStringList cp_MediaBibliothek::getAlbums(QString Interpret)
{
  QStringList Album;
  for (int x = 0; x < m_MusikBiblothek.count(); x++)
    {
      if (Interpret.compare("[All]") == 0)
        Album.append(m_MusikBiblothek.at(x).Album);
       else if (m_MusikBiblothek.at(x).Interpret.compare(Interpret) == 0)
        Album.append(m_MusikBiblothek.at(x).Album);
    }
  Album.sort();
  Album.removeDuplicates();
  Album.insert(0,"[All]");
  return Album;
}

/**************************************************************************
 * @brief       Ermittelt Titel eines Interpret und Album der MediaBibliothek
 * @autor       Sascha Deeg
 * @date        28.10.2010
 * @param       Interpret Namen des Interpret f�r den die Titel ermittelt
 *              werden sollen.
 * @param       Album Namen des Album f�r den die Titel ermittelt
 *              werden sollen.
 * @return      Gibt alle Title aus der Mediabibliothek als Liste zur�ck
 ************************************************************************* */
QStringList cp_MediaBibliothek::getTitles(QString Interpret, QString Album)
{
  QStringList Title;
  for (int x = 0; x < m_MusikBiblothek.count(); x++)
    {
      if (Interpret.compare("[All]") == 0 && Album.compare("[All]") == 0)
        {
          Title.append(m_MusikBiblothek.at(x).Title);
        }
      else if (Interpret.compare("[All]") == 0)
        {
          if (m_MusikBiblothek.at(x).Album.compare(Album) == 0)
            Title.append(m_MusikBiblothek.at(x).Title);
        }
      else if (Album.compare("[All]") == 0)
        {
          if (m_MusikBiblothek.at(x).Interpret.compare(Interpret) == 0)
            Title.append(m_MusikBiblothek.at(x).Title);
        }
      else
        {
          if (m_MusikBiblothek.at(x).Interpret.compare(Interpret) == 0
              && m_MusikBiblothek.at(x).Album.compare(Album) == 0)
            Title.append(m_MusikBiblothek.at(x).Title);
        }

    }
  return Title;
}

/**************************************************************************
 * @brief       Ermittelt Dateinamen der Titel eines Interpret und Album der MediaBibliothek
 * @autor       Sascha Deeg
 * @date        28.10.2010
 * @param       Interpret Namen des Interpret f�r den die Titel Dateinamen
 *              ermittelt werden sollen.
 * @param       Album Namen des Album f�r den die Titel Dateinamen ermittelt
 *              werden sollen.
 * @return      Gibt alle Alben aus der Mediabibliothek als Liste zur�ck
 ************************************************************************* */
QStringList cp_MediaBibliothek::getTitlesSource(QString Interpret, QString Album)
{
  QStringList Title;
  for (int x = 0; x < m_MusikBiblothek.count(); x++)
    {
      if (Interpret.compare("[All]") == 0 && Album.compare("[All]") == 0)
        {
          Title.append(m_MusikBiblothek.at(x).Source);
        }
      else if (Interpret.compare("[All]") == 0)
        {
          if (m_MusikBiblothek.at(x).Album.compare(Album) == 0)
            Title.append(m_MusikBiblothek.at(x).Source);
        }
      else if (Album.compare("[All]") == 0)
        {
          if (m_MusikBiblothek.at(x).Interpret.compare(Interpret) == 0)
            Title.append(m_MusikBiblothek.at(x).Source);
        }
      else
        {
          if (m_MusikBiblothek.at(x).Interpret.compare(Interpret) == 0
              && m_MusikBiblothek.at(x).Album.compare(Album) == 0)
            Title.append(m_MusikBiblothek.at(x).Source);
        }

    }
  return Title;

}

/**************************************************************************
 * @brief       Ermittelt alle Informationen zu einem Titel in der MediaBibliothek
 * @autor       Sascha Deeg
 * @date        28.10.2010
 * @param       Source des Titels f�r den die Informationen ermittelt
 *              werden sollen.
 * @return      Gibt Informationen der Musikdatei als CCARPCMusikMediaObject zur�ck
 ************************************************************************* */
const cp_ID3Object * cp_MediaBibliothek::getTitleInformation(QString Source)
{
  for (int x = 0; x < m_MusikBiblothek.count(); x++)
    {
      if (m_MusikBiblothek.at(x).Source.compare(Source) == 0)
        {
          return (&(m_MusikBiblothek.at(x)));
        }

    }
  return NULL;
}

bool cp_MediaBibliothek::ReadMediaBibliothekFile(QString Filename)
{
  QFile file(Filename);
     if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
          return false;

      while (!file.atEnd()) {

        QByteArray line = file.readLine();
          QString strLine(line);
          QString values[6];
          int i_stPos = 0;
          bool bOK = true;

          for(int x = 0;x <6;x++)
           {
              int i_Pos = strLine.indexOf(';',i_stPos);
              if(i_Pos > 0)
              {
                values[x] = strLine.mid(i_stPos,(i_Pos-i_stPos));
                i_stPos = i_Pos + 1;

              }
              else
              {
                bOK = false;
                break;
              }
           }
          //
          if(bOK)
          {
              cp_ID3Object tmpMO;
              tmpMO.Interpret = values[0];
              tmpMO.Album = values[1];
              tmpMO.Title = values[2];
              bool b_iOk;
              tmpMO.TrackNr = values[3].toInt(&b_iOk,10);
              tmpMO.Beschreibung = values[4];
              tmpMO.Source = values[5];
              m_MusikBiblothek.append(tmpMO);


          }

      }
      file.close();
      return true;
}

void cp_MediaBibliothek::GenerateMediaBiblothekFile()
{
  int l = m_MusikBiblothek.count();
  if (m_MusikBiblothek.count() <= 0)
    {
       return ;
    }
  QFile file("Media.cpbibl");
     if (!file.open(QIODevice::ReadWrite | QIODevice::Truncate| QIODevice::Text))
          return ;

    for(int x =0; x < m_MusikBiblothek.count(); x++)
    {
      file.write(m_MusikBiblothek.at(x).Interpret.toAscii());
      file.write(";");

      file.write(m_MusikBiblothek.at(x).Album.toAscii());
      file.write(";");

      file.write(m_MusikBiblothek.at(x).Title.toAscii());
      file.write(";");

      file.write(QString::number(m_MusikBiblothek.at(x).TrackNr,10).toAscii());
      file.write(";");

      file.write(m_MusikBiblothek.at(x).Beschreibung.toAscii());
      file.write(";");

      file.write(m_MusikBiblothek.at(x).Source.toAscii());
      file.write(";\r\n");
    }
    file.close();


}

QStringList cp_MediaBibliothek::getMovieSource()
{
  QStringList MovieSource;
  for (int x = 0; x < m_MovieBiblothek.count(); x++)
    {
      MovieSource.append(m_MovieBiblothek.at(x).Source);
    }
  return MovieSource;
}

QStringList cp_MediaBibliothek::getAllMovies()
{
  QStringList Movie;
  for (int x = 0; x < m_MovieBiblothek.count(); x++)
    {
    Movie.append(m_MovieBiblothek.at(x).Title);
    }
  return Movie;
}


