/**************************************************************************
 * @file        cp_MediaPlayList.h
 * @brief       Headerdatei der cp_MediaPlayList
 * @autor       Sascha Deeg
 * @date        15.06.2011
 *
 * Projekt      CARPC Pro
 * Modulkürzel: CARPCMM (MM = Multimedia)
 ************************************************************************* */

#include <qstring.h>
#include <qstringlist.h>
#ifndef CP_MEDIAPLAYLIST_H_
#define CP_MEDIAPLAYLIST_H_

class cp_MediaPlayList {
public:
	cp_MediaPlayList(QString Filename);
	cp_MediaPlayList(QString Filename,QString strName);
	cp_MediaPlayList(QString Filename,QString strName, QStringList Tracks);
	virtual ~cp_MediaPlayList();
private:
	QString m_strName;
	QString m_strFilename;
	QStringList m_Tracks;
	bool m_exists;
public:
	QString getName();
	bool LoadPlayList();
	void setName(QString strName);
	void addTrack(QString strPath);
	void deleteTrack(QString strPath);
	bool isUseable();
	bool existsPlayListFile();
	QString getTrack(int iNumber);
	QStringList getAllTracks();
	bool SavePlayList();
	bool SavePlayListAS(QString strFilename);
	void changePos(int iOldPos,int iNewPos);
	void upPos(int iOldPos);
	void downPos(int iOldPos);
};

#endif /* CP_MEDIAPLAYLIST_H_ */
