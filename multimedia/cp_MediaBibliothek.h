/**************************************************************************
 * @file        cp_MediaBibliothek.h
 * @brief       Headerdatei der cp_MediaBibliothek
 * @autor       Sascha Deeg
 * @date        28.10.2010
 *
 * Projekt      CARPC Pro
 * Modulkürzel: CARPCMM (MM = Multimedia)
 ************************************************************************* */

#ifndef CP_MEDIABIBLIOTHEK_H_
#define CP_MEDIABIBLIOTHEK_H_

#include "qstring.h"
#include <qlist.h>
#include <qstringlist.h>
#include "cp_ID3TAGReader.h"
#include "../file/CCARPCFileFinder.h"
#include "../basis/CLog.h"
#include "../basis/CSettings.h"
#include "cp_MediaPlayList.h"



class cp_MediaBibliothek {

public:
   static cp_MediaBibliothek * getInstance();
   static void Destroy();
   QStringList getAllAlbum();
   QStringList getAllInterpreten();
   QStringList getAlbums(QString Interpret);
   QStringList getTitles(QString Interpret,QString Album);
   QStringList getTitlesSource(QString Interpret,QString Album);
   QStringList getAllMovies();
   QStringList getMovieSource();
   void LoadMoviesinMediaBibliothek();
   void LoadPlayListsinMediaBibliothek();
   bool ReadMediaBibliothekFile(QString Filename);
   void GenerateMediaBiblothekFile();
   void LoadMusikFileinMediaBibliothek();
   QStringList getPlayLists();
   QStringList getPlaylistTitles(QString strPlaylist);
   QStringList getPlaylistTitlesSource(QString strPlaylist);
   const cp_ID3Object * getTitleInformation(QString Source);
private:
	static cp_MediaBibliothek * instance;
	cp_MediaBibliothek();
	virtual ~cp_MediaBibliothek();
	QList <cp_ID3Object> m_MusikBiblothek;
	QList <cp_ID3Object> m_MovieBiblothek;
	QList <cp_MediaPlayList> m_Playlists;


};

#endif /* cp_MediaBibliothek_H_ */
