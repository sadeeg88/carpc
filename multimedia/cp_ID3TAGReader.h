/*
 * cp_ID3TAGReader.h
 *
 *  Created on: 09.01.2011
 *      Author: Sascha
 */

#ifndef CP_ID3TAGREADER_H_
#define CP_ID3TAGREADER_H_
#include <qstring.h>

class cp_ID3Object
{
public:
        QString Interpret;
        QString Album;
        QString Title;
        int TrackNr;
        QString Beschreibung;
        QString Source;
};


class cp_ID3TAGReader
{
public:
  cp_ID3TAGReader();
  virtual
  ~cp_ID3TAGReader();
  cp_ID3Object ReadID3Tag(QString strFilename);

};

#endif /* CP_ID3TAGREADER_H_ */
