/*
 * CCARPCIDENCODE.h
 *
 *  Created on: 30.10.2010
 *      Author: Sascha
 */
#include <id3/tag.h>
#ifndef CCARPCIDENCODE_H_
#define CCARPCIDENCODE_H_

#ifdef WIN32
/**
 * This will be set for id3lib versions with Unicode bugs.
 * ID3LIB_ symbols cannot be found on Windows ?!
 */
#define UNICODE_SUPPORT_BUGGY 1
#else
/** This will be set for id3lib versions with Unicode bugs. */
#define UNICODE_SUPPORT_BUGGY 0
//((((ID3LIB_MAJOR_VERSION) << 16) + ((ID3LIB_MINOR_VERSION) << 8) + (ID3LIB_PATCH_VERSION)) <= 0x030803)
#endif

QString ID3FieldToQString(ID3_Field * field)
{
  QString ret;
  ID3_TextEnc enc = field->GetEncoding();
  if (enc == ID3TE_UTF16 || enc == ID3TE_UTF16BE)
    {
      const unicode_t *txt = field->GetRawUnicodeText();
      uint unicode_size = field->Size() / sizeof(unicode_t);
      if (unicode_size && txt)
        {
          QChar *qcarray = new QChar[unicode_size];
          if (qcarray)
            {
              // Unfortunately, Unicode support in id3lib is rather buggy
              // in the current version: The codes are mirrored.
              // In the hope that my patches will be included, I try here
              // to work around these bugs.
              uint i;
              for (i = 0; i < unicode_size; i++)
                {
                  qcarray[i] = UNICODE_SUPPORT_BUGGY ? (ushort) (((txt[i]
                      & 0x00ff) << 8) | ((txt[i] & 0xff00) >> 8))
                      : (ushort) txt[i];
                }
            //  if(UNICODE_SUPPORT_BUGGY)
              // {
              ret = QString(qcarray, unicode_size);
               //}
              //else
               // {
               // ret = QString(field->GetRawText());
               // }
              delete[] qcarray;
            }
        }
    }
  else
    {
      // (ID3TE_IS_SINGLE_BYTE_ENC(enc))
      // (enc == ID3TE_ISO8859_1 || enc == ID3TE_UTF8)
      ret = QString(field->GetRawText());
    }
  return ret;
}


#endif /* CCARPCIDENCODE_H_ */
