/**************************************************************************
 * @file        cp_MediaPlayer.h
 * @brief       Headerdatei des cp_MediaPlayer
 * @autor       Sascha Deeg
 * @date        27.11.2009
 *
 * Projekt      CARPC Pro
 * Modulkürzel: CARPCMM (MM = Multimedia)
 ************************************************************************* */

#include <phonon/mediacontroller.h>
#include <phonon/mediaobject.h>
#include <phonon/audiooutput.h>
#include <qstringlist.h>

enum cp_MediaPlayerMode
{
MP3,
CD,
DVD,
Video
};

enum CCARPCMediaRM
{
	repeat_none,
	repeat_title,
	repeat_all
};

#ifndef cp_MediaPlayer_H_
#define cp_MediaPlayer_H_




class cp_MediaPlayer: public QObject{
	Q_OBJECT
private:
	static cp_MediaPlayer * instance;
	cp_MediaPlayer();
	virtual ~cp_MediaPlayer();
	//Phonon::MediaObject * m_pMediaObjekt;
	Phonon::MediaController * m_pMediaController;
	Phonon::AudioOutput* m_pAudioOutput;
	bool m_bMuted;
	QStringList m_ListPlayList;
	int m_iCurrentTitle;
	cp_MediaPlayerMode currentMode;







public:
	Phonon::MediaObject * m_pMediaObjekt;

   static cp_MediaPlayer * getInstance();
   static void Destroy();
   void setPlayList(QStringList PlayList,int Title /*=0*/,cp_MediaPlayerMode mode/* = MP3*/);
   void Play();
   void SkipForward();
   void SkipBack();
   void SkipToTitle(int TitleNr);
   //Volume
   void VolumeUp(int iProzent);
   void VolumeDown(int iProzent);
   void setVolume(int Prozent);
   int getVolume();
   qreal getVolumeinDezibel();
   bool getMuted();
   QString getArtistTitleasQString();
   void Muted(bool bMuted);
   int getCurrentTitle();
   CCARPCMediaRM m_repeatMode;
   void clear();

public slots:
	void nextSong();
	void SaveCurrentPlay();
	void LoadCurrentPlay();

private slots:
        void StateChange(Phonon::State newstate, Phonon::State oldstate);


};

#endif /* cp_MediaPlayer_H_ */
