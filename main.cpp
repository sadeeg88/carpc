#include "gui/ccarpc_dlgMain.h"

#include <QtGui>
#include <QApplication>
#include <qstringlist.h>
#include "basis/CLog.h"
#include "basis/CCARPCDBConnection.h"
#include "basis/cp_Application.h"
#include <qfile.h>
#include "multimedia/cp_MediaPlayer.h"
#include "file/CCARPCFileFinder.h"
#include "multimedia/cp_MediaBibliothek.h"
#include "gui/ccarpctouchkeyboard_context.h"
#include "gui/cp_dlgMainMenu.h"
#include "ui_test.h"
#include "gui/ccarpcstyle.h"
#include "basis/cp_SystemControl.h"
#include <iostream>
#include "multimedia/cp_MediaPlayList.h"




int main(int argc, char *argv[])
{
cp_Application a(argc, argv);
a.setApplicationName("CARPC_Projekt");
CLog::getInstance()->Log(QString("Starte Anwendung"),LOG_DEBUGDEVELOP);

    QTime * Time = new QTime(QTime::currentTime());
    if(Time->hour() > 22 || Time->hour() < 6)
    {
    	ccarpcstyle::getInstance()->setNightMode(true);
    }



    //Unix SYSTEM
    cp_SystemControl::getInstance();
    #ifdef Q_WS_X11
        setup_unix_signal_handlers();
    #endif

    //

    ccarpctouchkeyboard_context *ic = new ccarpctouchkeyboard_context;
    a.setInputContext(ic);

    /*MediaBibbliothek Initzalisierung*/
    cp_MediaBibliothek * media = cp_MediaBibliothek::getInstance();
    if(!media->ReadMediaBibliothekFile("Media.cpbibl"))
     {
    	media->LoadMusikFileinMediaBibliothek();
     }
    media->LoadMoviesinMediaBibliothek();
    media->LoadPlayListsinMediaBibliothek();


    /*Ende MediaBibbliothek Initzalisierung*/


    cp_dlgMainMenu w(NULL);
    w.show();
    int ret = a.exec();
  CLog::getInstance()->Log(QString("Beende Anwendung"),LOG_DEBUGDEVELOP);
   return ret;
}
