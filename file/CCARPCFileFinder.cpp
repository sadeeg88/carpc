
/*
 * CCARPCFileFinder.cpp
 *
 *  Created on: 27.01.2010
 *      Author: Sascha
 */
#include <qstring.h>
#include <qfileinfo.h>
#include <qdir.h>
#include "CCARPCFileFinder.h"
#include "../basis/CLog.h"


CCARPCFileFinder::CCARPCFileFinder(QString strRootPath) {
	m_strRootPath = strRootPath;

}

CCARPCFileFinder::~CCARPCFileFinder() {
	// TODO Auto-generated destructor stub
}

QFileInfoList CCARPCFileFinder::findFiles(QString strFilter)
{
	return findFiles(strFilter,m_strRootPath);
}

QFileInfoList CCARPCFileFinder::findFiles(QString strFilter,QString strPath)
{
	QDir dirPath(strPath);
	QFileInfoList FileList;
	QFileInfoList DirList = dirPath.entryInfoList(QDir::Dirs|QDir::NoDotAndDotDot,QDir::Name);
	CLog::getInstance()->Log(strPath,LOG_DEBUGDEVELOP);
	 for (int i = 0; i < DirList.size(); ++i)
	 {
		 QFileInfo FIAktuell = DirList.at(i);
		 if(!FIAktuell.isReadable())
		 {

		 }
		 else if(FIAktuell.isDir())
		 {
			 FileList.append( findFiles(strFilter,FIAktuell.absoluteFilePath()));
		 }
		 else if(FIAktuell.isFile())
		 {
			 FileList.append(FIAktuell);
		 }

	 }

		QDir FilePath(strPath,strFilter);
		QFileInfoList FileDirList = FilePath.entryInfoList(QDir::Files|QDir::NoDotAndDotDot,QDir::Name);
		 for (int i = 0; i < FileDirList.size(); ++i)
		 {
			 QFileInfo FIAktuell = FileDirList.at(i);
			 if(!FIAktuell.isReadable())
			 {

			 }
			 else if(FIAktuell.isFile())
			 {
				 FileList.append(FIAktuell);
			 }

		 }

	return FileList;

}
