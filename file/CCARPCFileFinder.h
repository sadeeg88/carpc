/*
 * CCARPCFileFinder.h
 *
 *  Created on: 27.01.2010
 *      Author: Sascha
 */

#ifndef CCARPCFILEFINDER_H_
#define CCARPCFILEFINDER_H_
#include "qfileinfo.h"

class CCARPCFileFinder {
public:
	CCARPCFileFinder(QString strRootPath);
	virtual ~CCARPCFileFinder();
	QFileInfoList findFiles(QString Filter);
private:
	QFileInfoList findFiles(QString Filter,QString strPath);
	QString m_strRootPath;


};

#endif /* CCARPCFILEFINDER_H_ */
