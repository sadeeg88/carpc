/*
 * cp_dlgVideoMenu.h
 *
 *  Created on: 05.02.2011
 *      Author: Sascha
 */
#include "cp_dlgStdMenu.h"

#ifndef CP_DLGVIDEOMENU_H_
#define CP_DLGVIDEOMENU_H_

class cp_dlgVideoMenu : public cp_dlgStdMenu
{
  Q_OBJECT;
public:
  cp_dlgVideoMenu(QWidget* parent);
  virtual
  ~cp_dlgVideoMenu();

 private slots:
   void DoAction(QString strItemName);
};

#endif /* CP_DLGVIDEOMENU_H_ */
