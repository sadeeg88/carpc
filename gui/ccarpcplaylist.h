#ifndef CCARPCPLAYLIST_H
#define CCARPCPLAYLIST_H

#include <QtGui/QDialog>
#include "ui_ccarpcplaylist.h"
#include "CCARPCGuiMediaController.h"

    enum CCARPCPLAYLISTTYP
    {
     CCARPC_PLAYLIST_ALBUM,
     CCARPC_PLAYLIST_INTERPRET,
     CCARPC_PLAYLIST_TITLE,
     CCARPC_PLAYLIST_MOVIES,
     CCARPC_PLAYLIST_PLAYLIST,
     CCARPC_PLAYLIST_PLAYLISTTITLE
    };

class CCARPCPlaylist : public QDialog,private Ui_CCARPCPlaylistClass
{
    Q_OBJECT;

public:
    CCARPCPlaylist(QWidget *parent = 0, CCARPCPLAYLISTTYP TYP = CCARPC_PLAYLIST_ALBUM,QString strInterpret = "[All]",QString strAlbum ="[All]");
    ~CCARPCPlaylist();
    CCARPCPLAYLISTTYP m_Typ;
    QString m_strInterpret;
    QString m_strAlbum;
    bool isPlay;
    int m_iScrollPos;
    int m_iScrollSteps;


private:
    CCARPCGuiMediaController * pGuiMediaController;
    void setupStyle();

public slots:
	 void ClickedItem(QListWidgetItem * item);
	 void ScrollUp();
	 void ScrollDown();
	 void Nightmode();
	 void ClearFilter();
	 void ChangeFilter(const QString & text);
};

#endif // CCARPCPLAYLIST_H
