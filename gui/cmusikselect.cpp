#include "cmusikselect.h"
#include <QPixmap>
#include <QTime>
#include <QTimer>
#include <QDate>
#include <QtGui>
#include <phonon/phonon>
#include "../basis/CLog.h"
#include "../multimedia/cp_MediaBibliothek.h"
#include "../multimedia/cp_MediaPlayer.h"
#include "ccarpcplaylist.h"
#include "../basis/CSettings.h"
#include "../gui/ccarpcstyle.h"

CMusikSelect::CMusikSelect(QWidget *parent)
: QDialog(parent)
{
CLog::getInstance()->Log(QString("CMusikSelect::CMusikSelect(QWidget *parent)"),LOG_DEBUGDEVELOP);
setupUi(this);
pGuiMediaController = new CCARPCGuiMediaController(this);
pGuiMediaController->setupMediaplayer(this);
connect(pbMediaAlben,SIGNAL(clicked()),this, SLOT(MediaAlbum()));
connect(pbMediaInterpret,SIGNAL(clicked()),this, SLOT(MediaInterpret()));
connect(pbCD,SIGNAL(clicked()),this, SLOT(PlayCD()));
connect(pGuiMediaController->btnCCARPCGuiMediaControllerNightMode,SIGNAL(clicked()),this, SLOT(Nightmode()));
if(CSettings::getInstance()->getSetting("FullScreen").compare("true") == 0)
{
	this->showFullScreen();
	this->move(0,0);
}
setupStyle();
}

CMusikSelect::~CMusikSelect()
{

}

void CMusikSelect::MediaAlbum()
{

	CCARPCPlaylist * mpDialog = new CCARPCPlaylist(this,CCARPC_PLAYLIST_ALBUM);
	mpDialog->show();
	mpDialog->exec();
	delete mpDialog;
	setupStyle();

}

void CMusikSelect::MediaInterpret()
{

	CCARPCPlaylist * mpDialog = new CCARPCPlaylist(this,CCARPC_PLAYLIST_INTERPRET);
	mpDialog->show();
	mpDialog->exec();
	delete mpDialog;
	setupStyle();

}

void CMusikSelect::Nightmode()
{
	ccarpcstyle::getInstance()->setNightMode(!ccarpcstyle::getInstance()->getNightMode());
	setupStyle();
}
void CMusikSelect::setupStyle()
{
    gbBibliothek->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";font: 75 16pt \"Comic Sans MS\";gridline-color: rgb(85, 0, 255);");
    pbMediaPlayList->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";font: 75 16pt \"Comic Sans MS\";gridline-color: rgb(85, 0, 255);");
    pbMediaInterpret->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";font: 75 16pt \"Comic Sans MS\";gridline-color: rgb(85, 0, 255);");
    pbMediaAlben->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";font: 75 16pt \"Comic Sans MS\";gridline-color: rgb(85, 0, 255);");
    gbUSB->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";font: 75 16pt \"Comic Sans MS\";gridline-color: rgb(85, 0, 255);");
    pbUSBPlayList->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";font: 75 16pt \"Comic Sans MS\";gridline-color: rgb(85, 0, 255);");
    pbUSBInterpret->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";font: 75 16pt \"Comic Sans MS\";gridline-color: rgb(85, 0, 255);");
    pbUSBAlben->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";font: 75 16pt \"Comic Sans MS\";gridline-color: rgb(85, 0, 255);");
    gbOther->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";font: 75 16pt \"Comic Sans MS\";gridline-color: rgb(85, 0, 255);");
    pbCD->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";font: 75 16pt \"Comic Sans MS\";gridline-color: rgb(85, 0, 255);");
	pGuiMediaController->setupStyle();
}

void CMusikSelect::PlayCD()
{
  QStringList dummy;
  cp_MediaPlayer::getInstance()->setPlayList(dummy,1,CD);
}
