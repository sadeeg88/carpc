/*
 * cp_dlgNavit.cpp
 *
 *  Created on: 25.07.2011
 *      Author: sascha
 */

#include "cp_dlgNavit.h"
#include "../basis/CLog.h"
#include "../basis/CSettings.h"
#include "../gui/ccarpcstyle.h"
#include <qstring.h>
#include <qdatetime.h>


cp_dlgNavit* cp_dlgNavit::instance = NULL;

cp_dlgNavit*
cp_dlgNavit::getInstance(QWidget *parent)
{
  if (!instance)
    instance = new cp_dlgNavit(parent);
  return instance;
}

void
cp_dlgNavit::Destroy()
{
  // static
  if (instance)
    {
      delete instance;
      instance = NULL;
    }
}
bool cp_dlgNavit::Exsist()
{
	  if (instance)
	    {
	      return true;
	    }
	return false;
}

cp_dlgNavit::cp_dlgNavit(QWidget *parent)
: QDialog(parent)
  {
	  if(CSettings::getInstance()->getSetting("Widescreen").compare("true") == 0)
	  {
	    this->resize(800,480);
	  }
	  else
	  {
	    this->resize(800,600);
	  }
	  pGuiMediaController = new CCARPCGuiMediaController(this);
	pGuiMediaController->setupMediaplayer(this);

	container = new QX11EmbedContainer(this);
	container->setGeometry(QRect(0, 30, 800, this->height()-30-95));
	container->show();
	QString wId = QString::number(container->winId());
	setenv("NAVIT_XID", wId.toAscii(), 1);
	process = new QProcess(container);
	process->start("navit");
	container->embedClient(container->winId());

	// 2 Sekunden Verzögerung da es sonst Probleme mit der Initzalisierung gibt!
	QTime dieTime = QTime::currentTime().addSecs(2);
	while (QTime::currentTime() < dieTime);
	container->embedClient(container->winId());
	connect(pGuiMediaController->btnCCARPCGuiMediaControllerNightMode,SIGNAL(clicked()),this, SLOT(Nightmode()));




}

cp_dlgNavit::~cp_dlgNavit() {
	delete pGuiMediaController;
	pGuiMediaController = NULL;
	delete process;
	process = NULL;
	delete container;
	container = NULL;
}

void cp_dlgNavit::navit_start()
{
	container->embedClient(container->winId());
}

void cp_dlgNavit::setupStyle()
{
  pGuiMediaController->setupStyle();
  this->setStyleSheet(ccarpcstyle::getInstance()->getDialogStyle());
}

void cp_dlgNavit::Nightmode()
{
        ccarpcstyle::getInstance()->setNightMode(!ccarpcstyle::getInstance()->getNightMode());
        setupStyle();
}
