#include "CCARPCGuiMediaController.h"
#include "ccarpcplaylist.h"
#include "../multimedia/cp_MediaBibliothek.h"
#include "../multimedia/cp_MediaPlayer.h"
#include "../basis/CLog.h"
#include "../basis/CSettings.h"
#include "../gui/ccarpcstyle.h"
#include "../gui/cp_dlgVideo.h"

CCARPCPlaylist::CCARPCPlaylist(QWidget *parent , CCARPCPLAYLISTTYP TYP,QString strInterpret,QString strAlbum)
    : QDialog(parent)
{
	this->setupUi(this);
	//lineEdit->setVisible(false);
	edtFilter->setStyleSheet("font: 12pt \"Comic Sans MS\";\nbackground: none;\nbackground-color: rgba(0, 0, 0, 80%);\ncolor:"+ccarpcstyle::getInstance()->getColor()+";");
	btnClearFilter->setStyleSheet("font: 12pt \"Comic Sans MS\";\nbackground: none;\nbackground-color: rgba(0, 0, 0, 80%);\ncolor:"+ccarpcstyle::getInstance()->getColor()+";");


	//Laden der Icons f�r die ScrollButtons
    QIcon iconUP;
    iconUP.addFile ( "picture/Up.png", QSize(76,76));
    btnSCUp->setIcon(iconUP);
    btnSCUp->setIconSize( QSize(76,76));
    btnSCUp->setStyleSheet(QApplication::translate("CCARPCGuiMediaController",
    		"     border-style: outset;\n"
    		"     border-width: 0px;\n"
    		"     border-color: black;\n"
                "     background: none;\n"
                "     border-color: none;\n"
    		"", 0, QApplication::UnicodeUTF8));
    QIcon icondown;
    icondown.addFile ( "picture/Down.png", QSize(76,76));
    btnSCDown->setIcon(icondown);
    btnSCDown->setIconSize( QSize(76,76));
    btnSCDown->setStyleSheet(QApplication::translate("CCARPCGuiMediaController",
    		"     border-style: outset;\n"
    		"     border-width: 0px;\n"
    		"     border-color: black;\n"
                "     background: none;\n"
                "     border-color: none;\n"
    		"", 0, QApplication::UnicodeUTF8));


	m_iScrollPos = 0;
	m_Typ = TYP;
	m_strAlbum = strAlbum;
	m_strInterpret = strInterpret;
	isPlay = false;
	pGuiMediaController = new CCARPCGuiMediaController(this);

	switch(TYP)
	{
	case CCARPC_PLAYLIST_ALBUM:
		if(strInterpret.compare("[All]") == 0)
		{
			listWidget->addItems(cp_MediaBibliothek::getInstance()->getAllAlbum());
		}
		else
		{
			listWidget->addItems(cp_MediaBibliothek::getInstance()->getAlbums(strInterpret));
		}
		break;
	case CCARPC_PLAYLIST_INTERPRET:
		listWidget->addItems(cp_MediaBibliothek::getInstance()->getAllInterpreten());
		break;
	case CCARPC_PLAYLIST_TITLE:
			listWidget->addItems(cp_MediaBibliothek::getInstance()->getTitles(strInterpret,strAlbum));
		break;
	case CCARPC_PLAYLIST_MOVIES:
	                        listWidget->addItems(cp_MediaBibliothek::getInstance()->getAllMovies());
	    break;
	case CCARPC_PLAYLIST_PLAYLIST:
							listWidget->addItems(cp_MediaBibliothek::getInstance()->getPlayLists());
		break;
	case CCARPC_PLAYLIST_PLAYLISTTITLE:
								listWidget->addItems(cp_MediaBibliothek::getInstance()->getPlaylistTitles(strInterpret));
		break;
	default:
	break;
	};

	this->setMinimumSize(QSize(10, 10));
	this->setMaximumSize(QSize(800, 600));

        if(CSettings::getInstance()->getSetting("Widescreen").compare("true") == 0)
         {
           this->resize(800,480);

            listWidget->setGeometry(QRect(0, 30, 591 +55+60, 480-95-30));
            btnSCUp->setGeometry(QRect(720, 80, 76, 76));
            btnSCDown->setGeometry(QRect(720, 390 - 120, 76, 76));
            m_iScrollSteps = 3;
         }
         else
         {
           this->resize(800,600);
           listWidget->setGeometry(QRect(5, 80, 700, 421));
           btnSCUp->setGeometry(QRect(720, 80, 76, 76));
           btnSCDown->setGeometry(QRect(720, 390, 76, 76));
           m_iScrollSteps = 5;
         }

        //listWidget->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\nbackground: none;\n"));
        pGuiMediaController->setupMediaplayer(this);

	//connect(listWidget,)
	connect(listWidget,SIGNAL(itemClicked(QListWidgetItem * )),this, SLOT(ClickedItem(QListWidgetItem * )));
	connect(edtFilter,SIGNAL(textChanged (const QString &)),this,SLOT(ChangeFilter(const QString &)));
	connect(btnSCDown,SIGNAL(clicked ( bool)),this,SLOT(ScrollDown()));
	connect(btnSCUp,SIGNAL(clicked ( bool)),this,SLOT(ScrollUp()));
	connect(btnClearFilter,SIGNAL(clicked ( bool)),this,SLOT(ClearFilter()));
	connect(pGuiMediaController->btnCCARPCGuiMediaControllerNightMode,SIGNAL(clicked()),this, SLOT(Nightmode()));
	if(CSettings::getInstance()->getSetting("FullScreen").compare("true") == 0)
	{
		this->showFullScreen();
		this->move(0,0);
	}
	setupStyle();
	QListWidgetItem *temp;
	for(int x = 0; x < listWidget->count();x++)
	  {
	   temp = listWidget->item(x);
	   if(x % 2 == 0 || x == 0)
	   temp->setBackground(QBrush(QColor(0, 0, 0, 127),Qt::SolidPattern));
	   else
	    temp->setBackground(QBrush(QColor(150, 150, 150, 60),Qt::SolidPattern));

	  }
	this->setStyleSheet(ccarpcstyle::getInstance()->getDialogStyle());

}

CCARPCPlaylist::~CCARPCPlaylist()
{

}

void CCARPCPlaylist::ClickedItem(QListWidgetItem * item)
{
	if(m_Typ == CCARPC_PLAYLIST_TITLE || m_Typ == CCARPC_PLAYLIST_MOVIES|| m_Typ ==CCARPC_PLAYLIST_PLAYLISTTITLE)
	{
	//listWidget->it
		CLog::getInstance()->Log(QString("Title"),LOG_DEBUGDEVELOP);
		int Count = 0;
		for(int x= 0; x < listWidget->count(); x++)
		{
		  if(listWidget->item(x) == item)
		  {
			  Count = x;
			  //listWidget->sc
			  break;
		  }
		}
		CLog::getInstance()->Log(QString().number(Count,10),LOG_DEBUGDEVELOP);
		if(!isPlay)
		{
		  if(m_Typ == CCARPC_PLAYLIST_TITLE)
		    {
                      cp_MediaPlayer::getInstance()->setPlayList(cp_MediaBibliothek::getInstance()->getTitlesSource(m_strInterpret,m_strAlbum),Count,MP3);
                      isPlay = true;
		    }
		  else if(m_Typ ==CCARPC_PLAYLIST_MOVIES)
		    {
                      cp_MediaPlayer::getInstance()->setPlayList(cp_MediaBibliothek::getInstance()->getMovieSource(),Count,Video);
                      cp_dlgVideo * dlgVideo = new cp_dlgVideo(this);
                       dlgVideo->show();
                      dlgVideo->exec();
                      delete dlgVideo;
                      isPlay = false;
		    }
		else if(m_Typ ==CCARPC_PLAYLIST_PLAYLISTTITLE)
			    {
	                      cp_MediaPlayer::getInstance()->setPlayList(cp_MediaBibliothek::getInstance()->getPlaylistTitlesSource(m_strInterpret),Count,MP3);
	                      isPlay = true;
			    }
		}
		else
		{
		  cp_MediaPlayer::getInstance()->SkipToTitle(Count);
		}



	}
	else if(m_Typ == CCARPC_PLAYLIST_ALBUM)
	{
		QString strAlbum = item->text();
		CCARPCPlaylist * mpDialog = new CCARPCPlaylist(this,CCARPC_PLAYLIST_TITLE,m_strInterpret,strAlbum);
		mpDialog->show();
		mpDialog->exec();
		delete mpDialog;
		setupStyle();
	}
	else if(m_Typ == CCARPC_PLAYLIST_INTERPRET)
	{
		QString strInterpret = item->text();
		CCARPCPlaylist * mpDialog = new CCARPCPlaylist(this,CCARPC_PLAYLIST_ALBUM,strInterpret,"[All]");
		mpDialog->show();
		mpDialog->exec();
		delete mpDialog;
		setupStyle();

	}
	else if(m_Typ == CCARPC_PLAYLIST_PLAYLIST)
	{
		QString strInterpret = item->text();
		CCARPCPlaylist * mpDialog = new CCARPCPlaylist(this,CCARPC_PLAYLIST_PLAYLISTTITLE,strInterpret,"");
		mpDialog->show();
		mpDialog->exec();
		delete mpDialog;
		setupStyle();

	}

}

void CCARPCPlaylist::ScrollUp()
{
	QListWidgetItem * CurrentItem = listWidget->item(m_iScrollPos-m_iScrollSteps);
	if(CurrentItem != NULL)
	{
	m_iScrollPos = m_iScrollPos-m_iScrollSteps;
	listWidget->scrollToItem(CurrentItem,QAbstractItemView::PositionAtTop);
	}
	else
	{
		listWidget->scrollToTop();
		m_iScrollPos = 0;
	}
}

void CCARPCPlaylist::ScrollDown()
{
	QListWidgetItem * CurrentItem = listWidget->item(m_iScrollPos+m_iScrollSteps);
	if(CurrentItem != NULL)
	{
	m_iScrollPos = m_iScrollPos+m_iScrollSteps;
	listWidget->scrollToItem(CurrentItem,QAbstractItemView::PositionAtTop);
	}
	else
	{
		listWidget->scrollToBottom();
		m_iScrollPos = listWidget->count()-1;
	}
}

void CCARPCPlaylist::Nightmode()
{
	ccarpcstyle::getInstance()->setNightMode(!ccarpcstyle::getInstance()->getNightMode());
	setupStyle();
}
void CCARPCPlaylist::setupStyle()
{
	listWidget->setStyleSheet("border-width: 0px; border-left-width: 4px;  border-right-width: 4px;  border-style: solid; border-color: rgba(150, 150, 150, 10%);\nbackground: none;\nbackground-color: rgba(0, 0, 0, 60%);\ncolor:"+ccarpcstyle::getInstance()->getColor()+";\nfont: 48pt \"Bitstream Charter\";selection-color:"+ccarpcstyle::getInstance()->getColor()+";selection-background-color:black;");
	pGuiMediaController->setupStyle();
	edtFilter->setStyleSheet("font: 12pt \"Comic Sans MS\";\nbackground: none;\nbackground-color: rgba(0, 0, 0, 80%);\ncolor:"+ccarpcstyle::getInstance()->getColor()+";");
	        btnClearFilter->setStyleSheet("font: 12pt \"Comic Sans MS\";\nbackground: none;\nbackground-color: rgba(0, 0, 0, 80%);\ncolor:"+ccarpcstyle::getInstance()->getColor()+";");

	this->setStyleSheet(ccarpcstyle::getInstance()->getDialogStyle());
}
void CCARPCPlaylist::ChangeFilter(const QString & text)
{
  QString strFilter = text;

  for(int x = 0; x < listWidget->count(); x++)
  {
    QListWidgetItem * item = listWidget->item(x);
    if(strFilter.compare("") == 0)
      item->setHidden(false);
    else
    {
      if(item->text().contains(strFilter,Qt::CaseInsensitive)||(item->text().compare("[All]")==0))
        item->setHidden(false);
      else
        item->setHidden(true);
    }


  }
}
void CCARPCPlaylist::ClearFilter()
{
  ChangeFilter("");
  edtFilter->clear();
}
