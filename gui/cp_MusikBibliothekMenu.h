/*
 * cp_MusikBibliothekMenu.h
 *
 *  Created on: 22.02.2011
 *      Author: Sascha
 */
#include "cp_dlgStdMenu.h"
#ifndef CP_MUSIKBIBLIOTHEKMENU_H_
#define CP_MUSIKBIBLIOTHEKMENU_H_

class cp_MusikBibliothekMenu: public cp_dlgStdMenu
{
  Q_OBJECT;
public:
  cp_MusikBibliothekMenu(QWidget* parent);
  virtual
  ~cp_MusikBibliothekMenu();
private slots:
  void DoAction(QString strItemName);
};

#endif /* CP_MUSIKBIBLIOTHEKMENU_H_ */
