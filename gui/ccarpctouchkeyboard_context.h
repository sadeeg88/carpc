/*
 * ccarpctouchkeyboard_context.h
 *
 *  Created on: 24.08.2010
 *      Author: Sascha
 */
#include "ccarpctouchkeyboard.h"
#include <qinputcontext.h>

#ifndef CCARPCTOUCHKEYBOARD_CONTEXT_H_
#define CCARPCTOUCHKEYBOARD_CONTEXT_H_

class MyInputPanel;

class ccarpctouchkeyboard_context : public QInputContext
{
    Q_OBJECT

public:
	ccarpctouchkeyboard_context();
	virtual ~ccarpctouchkeyboard_context();
    bool filterEvent(const QEvent* event);

    QString identifierName();
    QString language();

    bool isComposing() const;

    void reset();

private slots:
    void sendCharacter(QChar character);

private:
    void updatePosition();

private:
    ccarpctouchkeyboard *inputPanel;
};

#endif /* CCARPCTOUCHKEYBOARD_CONTEXT_H_ */
