/*
 * cp_dlgMainMenu.h
 *
 *  Created on: 20.02.2011
 *      Author: Sascha
 */
#include "cp_dlgStdMenu.h"

#ifndef CP_DLGSETTINGMENU_H_
#define CP_DLGSETTINGMENU_H_

class cp_dlgSettingMenu : public cp_dlgStdMenu
{
  Q_OBJECT;

public:
        cp_dlgSettingMenu(QWidget* parent);
	virtual ~cp_dlgSettingMenu();
private slots:
  void DoAction(QString strItemName);
};

#endif /* CP_DLGMAINMENU_H_ */
