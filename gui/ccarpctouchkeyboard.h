/*
 * ccarpctouchkeyboard.h
 *
 *  Created on: 07.08.2010
 *      Author: Sascha
 */
#include <qwidget.h>
#include <qpushbutton.h>
#include <qapplication.h>
#include <qsignalmapper.h>

#ifndef CCARPCTOUCHKEYBOARD_H_
#define CCARPCTOUCHKEYBOARD_H_

class ccarpctouchkeyboard : public QWidget {
	Q_OBJECT

public:
	ccarpctouchkeyboard();
	virtual ~ccarpctouchkeyboard();
	QPushButton * keys[35];
	signals:
	    void characterGenerated(QChar character);

	protected:
	    bool event(QEvent *e);

	private slots:
	    void saveFocusWidget(QWidget *oldFocus, QWidget *newFocus);
	    void buttonClicked(QWidget *w);
	    void UpperCase();
	    void ChangeKeys();

	private:
	    QWidget *lastFocusedWidget;
	    QSignalMapper signalMapper;
	    bool bUpperCase;
	    int iStatus;
	    QString strTastenZeichen[3];
};

#endif /* CCARPCTOUCHKEYBOARD_H_ */
