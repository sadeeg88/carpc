#include "ccarpc_dlgMain.h"
#include <QPixmap>
#include <QTime>
#include <QTimer>
#include <QDate>
#include <phonon/phonon>
#include "../basis/CLog.h"
#include "../multimedia/cp_MediaPlayer.h"
#include "../gui/cmusikselect.h"
#include "../gui/cp_dlgVideoMenu.h"
#include "../basis/CSettings.h"
#include "../gui/ccarpcstyle.h"
#include "../gui/ccarpctouchkeyboard.h"
#include "../basis/CCARPCDBConnection.h"


//#include "stdio.h"

 
ccarpc_dlgMain::ccarpc_dlgMain(QWidget *parent)
    : QWidget(parent)
{
	CLog::getInstance()->Log(QString("CarPc::CarPc(QWidget *parent)"),LOG_DEBUGDEVELOP);
	setupUi(this);
	pGuiMediaController = new CCARPCGuiMediaController(this);
	pGuiMediaController->setupMediaplayer(this);
	loadLogo();
	connect(pushButton,SIGNAL(clicked()),this, SLOT(GoToMSelect()));
	connect(btnVideo,SIGNAL(clicked()),this, SLOT(clickedVideo()));
	connect(pGuiMediaController->btnCCARPCGuiMediaControllerNightMode,SIGNAL(clicked()),this, SLOT(Nightmode()));

	if(CSettings::getInstance()->getSetting("FullScreen").compare("true") == 0)
	{
		this->showFullScreen();
		this->move(0,0);
	}
	setupStyle();
	CLog::getInstance()->Log("Drivers:",LOG_DEBUGDEVELOP);
	//QStringList l =  QSqlDatabase::drivers ();
	//for(int x = 0;x < l.length();x++)
	//{
	//CLog::getInstance()->Log(l.at(x),LOG_DEBUGDEVELOP);
	//}

    /*CCARPCDBConnection db;
    if(db.open())
    {
    	CLog::getInstance()->Log(QString("DB OK"),LOG_DEBUGDEVELOP);
    	QStringList list = db.tables(QSql::Tables);
    	CLog::getInstance()->Log(QString("Tabellen:")+QString::number(list.length(),10),LOG_DEBUGDEVELOP);
    }
    else
    {
    	CLog::getInstance()->Log(QString("DB FEHLER"),LOG_DEBUGDEVELOP);
    	QSqlError error = db.lastError();
    	CLog::getInstance()->Log(error.text(),LOG_DEBUGDEVELOP);
    }*/
}

ccarpc_dlgMain::~ccarpc_dlgMain()
{
	CLog::getInstance()->Log(QString("CarPc::~CarPc()"),LOG_DEBUGDEVELOP);

}

/*
 * Autor: Sascha Deeg
 * Version: 1.0
 * Description: Diese Funktion liest das Logo aus einer
 * 				Datei ein und zeigt es in dem Objekt lbLogo an.
 * Changes: 28.06.09 Deeg,Sascha  Erstellung der Funktion
 */
void ccarpc_dlgMain::loadLogo()
{
	CLog::getInstance()->Log(QString("CarPc::loadLogo()"),LOG_DEBUGDEVELOP);
	QPixmap picture;
    picture.load("picture/logo.png");
    lblogo->setPixmap(picture);
}

void ccarpc_dlgMain::GoToMSelect()
{
	//ccarpctouchkeyboard * t = new ccarpctouchkeyboard(this);
	//t->show();

	CMusikSelect * mpDialog = new CMusikSelect(this);
	mpDialog->show();
	mpDialog->exec();
	delete mpDialog;
	setupStyle();
}


void ccarpc_dlgMain::VolumeUp()
{
  cp_MediaPlayer::getInstance()->VolumeUp(10);
}
void ccarpc_dlgMain::VolumeDown()
{
  cp_MediaPlayer::getInstance()->VolumeDown(10);
}
void ccarpc_dlgMain::Nightmode()
{
	ccarpcstyle::getInstance()->setNightMode(!ccarpcstyle::getInstance()->getNightMode());
	setupStyle();
}
void ccarpc_dlgMain::setupStyle()
{
    pushButton->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";");
    pushButton_2->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";");
    pushButton_3->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";");
    pushButton_4->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";");
    btnVideo->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";");
    pushButton_6->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";");
	pGuiMediaController->setupStyle();
}

void ccarpc_dlgMain::clickedVideo()
{
  cp_dlgVideoMenu * dlgVideo = new cp_dlgVideoMenu(this);
    dlgVideo->show();
    dlgVideo->exec();
    delete dlgVideo;
    setupStyle();
  /*cp_dlgVideo * dlgVideo = new cp_dlgVideo(this);
  dlgVideo->show();
  dlgVideo->exec();
  delete dlgVideo;
  setupStyle();*/

}

