/*
 * ccarpcstyle.h
 *
 *  Created on: 08.07.2010
 *      Author: Sascha
 */
#include <qstring.h>
#include "../basis/CSettings.h"

#ifndef CCARPCSTYLE_H_
#define CCARPCSTYLE_H_

class ccarpcstyle {
private:
	static ccarpcstyle * ccarpcstyle_instance;
	ccarpcstyle();
	virtual ~ccarpcstyle();
	bool bNightmode;


public:
   static ccarpcstyle * getInstance();
   static void Destroy();
   QString getColor();
   QString getDialogStyle();
   void setNightMode(bool ngMode);

   bool getNightMode();
};

#endif /* CCARPCSTYLE_H_ */
