/*
 * cp_dlgMainMenu.h
 *
 *  Created on: 20.02.2011
 *      Author: Sascha
 */
#include "cp_dlgStdMenu.h"

#ifndef CP_DLGMAINMENU_H_
#define CP_DLGMAINMENU_H_

class cp_dlgMainMenu : public cp_dlgStdMenu
{
  Q_OBJECT;

public:
	cp_dlgMainMenu(QWidget* parent);
	virtual ~cp_dlgMainMenu();
private slots:
  void DoAction(QString strItemName);
};

#endif /* CP_DLGMAINMENU_H_ */
