/*
 * cp_dlgStdMenu.cpp
 *
 *  Created on: 04.02.2011
 *      Author: Sascha
 */

#include "cp_dlgStdMenu.h"
#include "../basis/CLog.h"
#include "../basis/CSettings.h"
#include "../gui/ccarpcstyle.h"


cp_dlgStdMenu::cp_dlgStdMenu(QWidget *parent)
: QDialog(parent)
{
  if(CSettings::getInstance()->getSetting("Widescreen").compare("true") == 0)
  {
    this->resize(800,480);
  }
  else
  {
    this->resize(800,600);
  }
  pGuiMediaController = new CCARPCGuiMediaController(this);
  pGuiMediaController->setupMediaplayer(this);

  QFont font1;
  font1.setFamily(QString::fromUtf8("Comic Sans MS"));
  font1.setPointSize(20);

  int iPosLeft,iPosTop,iWidth,iHeight;

  iWidth = 224;
  iHeight = 224;
  iPosTop = (((this->size().height())/2) - (iHeight / 2) -40);
  iPosLeft= ((this->size().width())/2) - (iWidth / 2);

  this->setStyleSheet(ccarpcstyle::getInstance()->getDialogStyle());

  btnSelect = new QPushButton(this);
  btnSelect->setFont(font1);
  btnSelect->setObjectName(QString::fromUtf8("btnSelect"));
  btnSelect->setGeometry(QRect(iPosLeft, iPosTop, iWidth, iHeight));
  btnSelect->setFont(font1);
  btnSelect->setIconSize( QSize(iWidth,iHeight));
  btnSelect->setStyleSheet(QApplication::translate("cp_dlgStdMenu",
              "     border-style: none;\n"
              "     border-width: 2px;\n"
              "     border-color: none;\n"
              "     background: none;\n"
              "", 0, QApplication::UnicodeUTF8));

   lbLabel = new QLabel(this);
   lbLabel->setFont(font1);
   lbLabel->setObjectName(QString::fromUtf8("lbLabel"));
   lbLabel->setGeometry(QRect(0, iPosTop + iHeight,this->size().width() , 50));
   lbLabel->setFont(font1);
   lbLabel->setStyleSheet(" border-style: none;\n border-width: 2px;\n border-color: none;\n color:"+ccarpcstyle::getInstance()->getColor()+";\nbackground: none;\n");
   lbLabel->setAlignment(Qt::AlignCenter);

  iWidth = 130;
  iHeight = 130;
  iPosLeft = iPosLeft - 130  - 75;
  iPosTop =  ((this->size().height())/2) - (iHeight / 2);

  btnSwitchLeft = new QPushButton(this);
  btnSwitchLeft->setFont(font1);
  btnSwitchLeft->setObjectName(QString::fromUtf8("btnSwitchLeft"));
  btnSwitchLeft->setGeometry(QRect(iPosLeft, iPosTop, iWidth, iHeight));
  btnSwitchLeft->setFont(font1);
  QIcon iconleft;
  iconleft.addFile ( "picture/menu_switchleft.png", QSize(iWidth,iHeight));
  btnSwitchLeft->setIcon(iconleft);
  btnSwitchLeft->setIconSize( QSize(iWidth,iHeight));
  btnSwitchLeft->setStyleSheet(QApplication::translate("cp_dlgStdMenu",
              "     border-style: none;\n"
              "     border-width: 2px;\n"
              "     background: none;\n"
              "     border-color: none;\n"
              "", 0, QApplication::UnicodeUTF8));

  iPosLeft = iPosLeft + 130  + 75 + 224 + 75;

  btnSwitchRight = new QPushButton(this);
  btnSwitchRight->setFont(font1);
  btnSwitchRight->setObjectName(QString::fromUtf8("btnSwitchRight"));
  btnSwitchRight->setGeometry(QRect(iPosLeft, iPosTop, iWidth, iHeight));
  btnSwitchRight->setFont(font1);
  QIcon iconright;
  iconright.addFile ( "picture/menu_switchright.png", QSize(iWidth,iHeight));
  btnSwitchRight->setIcon(iconright);
  btnSwitchRight->setIconSize( QSize(iWidth,iHeight));
  btnSwitchRight->setStyleSheet(QApplication::translate("cp_dlgStdMenu",
              "     border-style: none;\n"
              "     border-width: 2px;\n"
              "     background: none;\n"
              "     border-color: none;\n"
              "", 0, QApplication::UnicodeUTF8));


  if(CSettings::getInstance()->getSetting("FullScreen").compare("true") == 0)
  {
          this->showFullScreen();
          this->move(0,0);
  }

  connect(btnSwitchRight,SIGNAL(clicked()),this, SLOT(NextItem()));
  connect(btnSwitchLeft,SIGNAL(clicked()),this, SLOT(PreviosItem()));
  connect(btnSelect,SIGNAL(clicked()),this, SLOT(ItemAction()));
  connect(pGuiMediaController->btnCCARPCGuiMediaControllerNightMode,SIGNAL(clicked()),this, SLOT(Nightmode()));



}

cp_dlgStdMenu::~cp_dlgStdMenu()
{
  delete pGuiMediaController;
  // TODO Auto-generated destructor stub
}

void cp_dlgStdMenu::ItemAction()
{
  emit ItemSelected(CurrentItem.m_strName);
}

void cp_dlgStdMenu::NextItem()
{
  int iPos = Items.indexOf(CurrentItem,0);
  if(iPos > -1)
  {
    if(iPos + 1 < Items.count())
    {
      CurrentItem = Items.at(iPos + 1);
      btnSelect->setIcon(CurrentItem.m_Icon);
      lbLabel->setText(CurrentItem.m_strDescription);

    }
    else
    {
      CurrentItem = Items.at(0);
      btnSelect->setIcon(CurrentItem.m_Icon);
      lbLabel->setText(CurrentItem.m_strDescription);
    }
  }

}
void cp_dlgStdMenu::PreviosItem()
{
  int iPos = Items.indexOf(CurrentItem,0);
   if(iPos > -1)
   {
     if(iPos - 1 >= 0 )
     {
       CurrentItem = Items.at(iPos - 1);
       btnSelect->setIcon(CurrentItem.m_Icon);
       lbLabel->setText(CurrentItem.m_strDescription);

     }
     else
     {
       CurrentItem = Items.at(Items.count() -1 );
       btnSelect->setIcon(CurrentItem.m_Icon);
       lbLabel->setText(CurrentItem.m_strDescription);
     }
   }

}

void cp_dlgStdMenu::SetupMenu()
{
  if( Items.count() > 0)
  {
    CurrentItem = Items.at(0);
    btnSelect->setIcon(CurrentItem.m_Icon);
    lbLabel->setText(CurrentItem.m_strDescription);
  }

}
void cp_dlgStdMenu::ResetMenuItems()
{
  Items.clear();
}
void cp_dlgStdMenu::AddMenuItem(QString strName,QString strIcon, QString strDescription)
{
  int iWidth = 224;
  int iHeight = 224;
  QIcon ItemIcon;
  ItemIcon.addFile ( strIcon, QSize(iWidth,iHeight));
  cp_menu_item MenuItem(strName,ItemIcon,strDescription);
  Items.append(MenuItem);
}

void cp_dlgStdMenu::Nightmode()
{
        ccarpcstyle::getInstance()->setNightMode(!ccarpcstyle::getInstance()->getNightMode());
        setupStyle();
}
void cp_dlgStdMenu::setupStyle()
{
       // listWidget->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";border-style:outset;border-color:rgb(0, 0, 255);border-size:2px;font: 48pt \"Bitstream Charter\";selection-color:"+ccarpcstyle::getInstance()->getColor()+";selection-background-color:black;");
  lbLabel->setStyleSheet("     border-style: none;\n     border-width: 2px;\n border-color: black;\n color:"+ccarpcstyle::getInstance()->getColor()+";\nbackground: none;\n");
  pGuiMediaController->setupStyle();
  this->setStyleSheet(ccarpcstyle::getInstance()->getDialogStyle());
}
