/*
 * cp_MusikBibliothekMenu.cpp
 *
 *  Created on: 22.02.2011
 *      Author: Sascha
 */

#include "cp_MusikBibliothekMenu.h"
#include "../gui/cp_dlgVideo.h"
#include "../gui/ccarpcplaylist.h"
#include "../multimedia/cp_MediaPlayer.h"
#include "../gui/cmusikselect.h"
#include "../gui/cp_dlgVideoMenu.h"

cp_MusikBibliothekMenu::cp_MusikBibliothekMenu(QWidget * parent)
:cp_dlgStdMenu(parent)
{
	this->AddMenuItem("Playlist","picture/cp_playlist.png","Playlist");
   this->AddMenuItem("Interpret","picture/cp_bibliomenu_artist.png","Interpret");
   this->AddMenuItem("Album","picture/cp_bibliomenu_album.png","Album");
   SetupMenu();
   connect(this,SIGNAL(ItemSelected(QString)),this, SLOT(DoAction(QString)));

}

cp_MusikBibliothekMenu::~cp_MusikBibliothekMenu()
{
  // TODO Auto-generated destructor stub
}
void cp_MusikBibliothekMenu::DoAction(QString strItemName)
{
  if(strItemName.compare("Interpret")==0)
    {
    CCARPCPlaylist * mpDialog = new CCARPCPlaylist(this,CCARPC_PLAYLIST_INTERPRET);
    mpDialog->show();
    mpDialog->exec();
    delete mpDialog;
    setupStyle();

    }
  else if (strItemName.compare("Album")==0)
    {
    CCARPCPlaylist * mpDialog = new CCARPCPlaylist(this,CCARPC_PLAYLIST_ALBUM);
    mpDialog->show();
    mpDialog->exec();
    delete mpDialog;
    setupStyle();
    }
  else if (strItemName.compare("Playlist")==0)
    {
    CCARPCPlaylist * mpDialog = new CCARPCPlaylist(this,CCARPC_PLAYLIST_PLAYLIST);
    mpDialog->show();
    mpDialog->exec();
    delete mpDialog;
    setupStyle();
    }


}
