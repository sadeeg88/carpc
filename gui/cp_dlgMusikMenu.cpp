/*
 * cp_dlgMusikMenu.cpp
 *
 *  Created on: 22.02.2011
 *      Author: Sascha
 */

#include "cp_dlgMusikMenu.h"
#include "cp_MusikBibliothekMenu.h"
#include "../gui/cp_dlgVideo.h"
#include "../gui/ccarpcplaylist.h"
#include "../multimedia/cp_MediaPlayer.h"
#include "../gui/cmusikselect.h"
#include "../gui/cp_dlgVideoMenu.h"

cp_dlgMusikMenu::cp_dlgMusikMenu(QWidget * parent)
:cp_dlgStdMenu(parent)
{
   this->AddMenuItem("Bibliothek","picture/cp_musicmenu_bibliothek.png","Bibliothek");
   this->AddMenuItem("USB","picture/cp_musicmenu_usb.png","USB");
   this->AddMenuItem("CD","picture/cp_musicmenu_cd.png","CD");
   //this->AddMenuItem("Radio","picture/cp_mainmenu_radio.png","Radio");
   //this->AddMenuItem("Settings","picture/cp_mainmenu_settings.png","Settings");
   SetupMenu();
   connect(this,SIGNAL(ItemSelected(QString)),this, SLOT(DoAction(QString)));

}

cp_dlgMusikMenu::~cp_dlgMusikMenu()
{
  // TODO Auto-generated destructor stub
}


void cp_dlgMusikMenu::DoAction(QString strItemName)
{
  if(strItemName.compare("Bibliothek")==0)
    {
                cp_MusikBibliothekMenu * mpDialog = new cp_MusikBibliothekMenu(this);
                mpDialog->show();
                mpDialog->exec();
                delete mpDialog;
                setupStyle();

    }
  else if (strItemName.compare("CD")==0)
    {
        QStringList dummy;
        cp_MediaPlayer::getInstance()->setPlayList(dummy,1,CD);
    }

 /* else if (strItemName.compare("VideoBibliothek")==0)
    {
    CCARPCPlaylist * mpDialog = new CCARPCPlaylist(this,CCARPC_PLAYLIST_MOVIES);
          mpDialog->show();
          mpDialog->exec();
          delete mpDialog;
          //setupStyle();

    }*/

}
