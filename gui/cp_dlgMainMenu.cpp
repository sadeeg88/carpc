/*
 * cp_dlgMainMenu.cpp
 *
 *  Created on: 20.02.2011
 *      Author: Sascha
 */

#include "cp_dlgMainMenu.h"
#include "../gui/cp_dlgMusikMenu.h"
#include "../gui/cp_dlgVideo.h"
#include "../gui/ccarpcplaylist.h"
#include "../multimedia/cp_MediaPlayer.h"
#include "../gui/cmusikselect.h"
#include "../gui/cp_dlgVideoMenu.h"
#include "../gui/cp_dlgSettingMenu.h"
#include "../gui/cp_dlgNavit.h"

cp_dlgMainMenu::cp_dlgMainMenu(QWidget * parent)
:cp_dlgStdMenu(parent)
{
   this->AddMenuItem("Musik","picture/cp_mainmenu_musik.png","Musik");
   this->AddMenuItem("Video","picture/videobib.png","Video");
#ifdef Q_WS_X11
   this->AddMenuItem("Navigation","picture/dvd.png","Navigation");
#endif
   //this->AddMenuItem("Radio","picture/cp_mainmenu_radio.png","Radio");
   this->AddMenuItem("Settings","picture/cp_mainmenu_settings.png","Settings");
   SetupMenu();
   connect(this,SIGNAL(ItemSelected(QString)),this, SLOT(DoAction(QString)));
   this->pGuiMediaController->btnCCARPCGuiMediaControllerQuit->setEnabled(false);
   this->pGuiMediaController->btnCCARPCGuiMediaControllerQuit->setHidden(true);


}

cp_dlgMainMenu::~cp_dlgMainMenu()
{
  // TODO Auto-generated destructor stub
}

void cp_dlgMainMenu::DoAction(QString strItemName)
{
  if(strItemName.compare("Musik")==0)
    {
                cp_dlgMusikMenu * mpDialog = new cp_dlgMusikMenu(this);
		mpDialog->show();
		mpDialog->exec();
		delete mpDialog;
		setupStyle();

    }
  else if (strItemName.compare("Video")==0)
    {
	  cp_dlgVideoMenu * dlgVideo = new cp_dlgVideoMenu(this);
	    dlgVideo->show();
	    dlgVideo->exec();
	    delete dlgVideo;
	    setupStyle();
    }

  else if (strItemName.compare("Settings")==0)
    {
    cp_dlgSettingMenu * dlgSetting = new cp_dlgSettingMenu(this);
    dlgSetting->show();
    dlgSetting->exec();
      delete dlgSetting;
      setupStyle();

    }
#ifdef Q_WS_X11
  else if (strItemName.compare("Navigation")==0)
    {
    cp_dlgNavit * dlgNavit= cp_dlgNavit::getInstance(this);
    dlgNavit->show();
    dlgNavit->exec();
     // delete dlgNavit;
      setupStyle();

    }
#endif

}
