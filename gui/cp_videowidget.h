/*
 * cp_videowidget.h
 *
 *  Created on: 30.01.2011
 *      Author: Sascha
 */
#include <videowidget.h>

#ifndef CP_VIDEOWIDGET_H_
#define CP_VIDEOWIDGET_H_

class cp_videowidget : public Phonon::VideoWidget
{
  Q_OBJECT
public:
  cp_videowidget(QWidget *parent);
 virtual
  ~cp_videowidget();
private:
    void mousePressEvent(QMouseEvent *event);

 signals:
       void clicked(void);
};

#endif /* CP_VIDEOWIDGET_H_ */
