/*
 * CCARPCGuiMediaController.cpp
 *
 *  Created on: 09.12.2009
 *      Author: Sascha
 */

#include "CCARPCGuiMediaController.h"
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>
#include <QPixmap>
#include <QTime>
#include <QTimer>
#include <QDate>
#include <phonon/phonon>
#include "../basis/CLog.h"
#include "../multimedia/cp_MediaPlayer.h"
#include <qobject.h>
#include "../basis/CSettings.h"
#include "../gui/ccarpcstyle.h"


CCARPCGuiMediaController::CCARPCGuiMediaController(QWidget *parent)
    : QObject(parent) {
	// TODO Auto-generated constructor stub

}

CCARPCGuiMediaController::~CCARPCGuiMediaController() {
	// TODO Auto-generated destructor stub
}

void CCARPCGuiMediaController::setupMediaplayer(QWidget *parent)
{
    int iControlYPos;

    if(CSettings::getInstance()->getSetting("Widescreen").compare("true") == 0)
    {
      iControlYPos = 519 - 120;
    }
    else
    {
      iControlYPos = 519;
    }

    lblbCCARPCGuiMediaControllerBackgroundButton = new QLabel(parent);
     lblbCCARPCGuiMediaControllerBackgroundButton->setObjectName(QString::fromUtf8("lbBackgroundButton"));
     lblbCCARPCGuiMediaControllerBackgroundButton->setGeometry(QRect(0, parent->geometry().height()-95, 800,95));
     lblbCCARPCGuiMediaControllerBackgroundButton->setStyleSheet("border-width: 0px; border-style: solid; border-color: rgba(150, 150, 150, 10%);\nborder-top-width: 4px;\nborder-left-width: 0px;\nborder-right-width: 0px;\nbackground: none;\nbackground-color: rgba(0, 0, 0, 80%);\ncolor:"+ccarpcstyle::getInstance()->getColor()+";");
     lblbCCARPCGuiMediaControllerBackgroundButton->setForegroundRole(QPalette::Background);

    QFont font1;
    font1.setFamily(QString::fromUtf8("Comic Sans MS"));
    font1.setPointSize(20);
    btnCCARPCGuiMediaControllerPlay = new QPushButton(parent);
    btnCCARPCGuiMediaControllerPlay->setFont(font1);
    btnCCARPCGuiMediaControllerPlay->setObjectName(QString::fromUtf8("btnCCARPCGuiMediaControllerPlay"));
    btnCCARPCGuiMediaControllerPlay->setGeometry(QRect(358, iControlYPos - 9, 85, 85));
    btnCCARPCGuiMediaControllerPlay->setFont(font1);
    QIcon icon;
    icon.addFile ( "picture/Play.png", QSize(85,85));
    btnCCARPCGuiMediaControllerPlay->setIcon(icon);
    btnCCARPCGuiMediaControllerPlay->setIconSize( QSize(85,85));
    btnCCARPCGuiMediaControllerPlay->setStyleSheet(QApplication::translate("CCARPCGuiMediaController",
    		"     border-style: outset;\n"
    		"     border-width: 0px;\n"
    		"     border-color: none;\n"
                "     background: none;\n"
    		"", 0, QApplication::UnicodeUTF8));


    btnCCARPCGuiMediaControllerSkipF = new QPushButton(parent);
    btnCCARPCGuiMediaControllerSkipF->setFont(font1);
    btnCCARPCGuiMediaControllerSkipF->setObjectName(QString::fromUtf8("btnCCARPCGuiMediaControllerPlay"));
    btnCCARPCGuiMediaControllerSkipF->setGeometry(QRect(448, iControlYPos - 5, 76, 76));
    btnCCARPCGuiMediaControllerSkipF->setFont(font1);
    QIcon iconSkipF;
    iconSkipF.addFile ( "picture/skipf.png", QSize(76,76));
    btnCCARPCGuiMediaControllerSkipF->setIcon(iconSkipF);
    btnCCARPCGuiMediaControllerSkipF->setIconSize( QSize(76,76));
    btnCCARPCGuiMediaControllerSkipF->setStyleSheet(QApplication::translate("CCARPCGuiMediaController",
        "     border-style: outset;\n"
        "     border-width: 0px;\n"
        "     border-color: none;\n"
        "     background: none;\n"
    		"", 0, QApplication::UnicodeUTF8));

    btnCCARPCGuiMediaControllerSkipB = new QPushButton(parent);
    btnCCARPCGuiMediaControllerSkipB->setFont(font1);
    btnCCARPCGuiMediaControllerSkipB->setObjectName(QString::fromUtf8("btnCCARPCGuiMediaControllerPlay"));
    btnCCARPCGuiMediaControllerSkipB->setGeometry(QRect(277, iControlYPos - 5, 76, 76));
    btnCCARPCGuiMediaControllerSkipB->setFont(font1);
    QIcon iconSkipB;
    iconSkipB.addFile ( "picture/skipb.png", QSize(76,76));
    btnCCARPCGuiMediaControllerSkipB->setIcon(iconSkipB);
    btnCCARPCGuiMediaControllerSkipB->setIconSize( QSize(76,76));
    btnCCARPCGuiMediaControllerSkipB->setStyleSheet(QApplication::translate("CCARPCGuiMediaController",
        "     border-style: outset;\n"
        "     border-width: 0px;\n"
        "     border-color: none;\n"
        "     background: none;\n"
    		"", 0, QApplication::UnicodeUTF8));


    btnCCARPCGuiMediaControllerVolumeDown = new QPushButton(parent);
    btnCCARPCGuiMediaControllerVolumeDown->setFont(font1);
    btnCCARPCGuiMediaControllerVolumeDown->setObjectName(QString::fromUtf8("btnCCARPCGuiMediaControllerPlay"));
    btnCCARPCGuiMediaControllerVolumeDown->setGeometry(QRect(641, iControlYPos, 67, 67));

    btnCCARPCGuiMediaControllerVolumeDown->setFont(font1);
    QIcon iconVolumeDown;
    iconVolumeDown.addFile ( "picture/volumedown.png", QSize(67,67));
    btnCCARPCGuiMediaControllerVolumeDown->setIcon(iconVolumeDown);
    btnCCARPCGuiMediaControllerVolumeDown->setIconSize( QSize(67,67));
    btnCCARPCGuiMediaControllerVolumeDown->setStyleSheet(QApplication::translate("CCARPCGuiMediaController",
        "     border-style: outset;\n"
        "     border-width: 0px;\n"
        "     border-color: none;\n"
        "     background: none;\n"
    		"", 0, QApplication::UnicodeUTF8));



    btnCCARPCGuiMediaControllerVolumeUp = new QPushButton(parent);
    btnCCARPCGuiMediaControllerVolumeUp->setFont(font1);
    btnCCARPCGuiMediaControllerVolumeUp->setObjectName(QString::fromUtf8("btnCCARPCGuiMediaControllerPlay"));
    btnCCARPCGuiMediaControllerVolumeUp->setGeometry(QRect(720, iControlYPos, 67, 67));
    btnCCARPCGuiMediaControllerVolumeUp->setFont(font1);
    QIcon iconVolumeUp;
    iconVolumeUp.addFile ( "picture/+.png", QSize(67,67));
    btnCCARPCGuiMediaControllerVolumeUp->setIcon(iconVolumeUp);
    btnCCARPCGuiMediaControllerVolumeUp->setIconSize( QSize(67,67));
    btnCCARPCGuiMediaControllerVolumeUp->setStyleSheet(QApplication::translate("CCARPCGuiMediaController",
        "     border-style: outset;\n"
        "     border-width: 0px;\n"
        "     border-color: none;\n"
        "     background: none;\n"
    		"", 0, QApplication::UnicodeUTF8));

    btnCCARPCGuiMediaControllerQuit = new QPushButton(parent);
    btnCCARPCGuiMediaControllerQuit->setFont(font1);
    btnCCARPCGuiMediaControllerQuit->setObjectName(QString::fromUtf8("btnCCARPCGuiMediaControllerQuit"));
    btnCCARPCGuiMediaControllerQuit->setGeometry(QRect(20, iControlYPos, 67, 67));
    btnCCARPCGuiMediaControllerQuit->setFont(font1);
    QIcon iconVolumeQuit;
    iconVolumeQuit.addFile ( "picture/back.png", QSize(67,67));
    btnCCARPCGuiMediaControllerQuit->setIcon(iconVolumeQuit);
    btnCCARPCGuiMediaControllerQuit->setIconSize( QSize(67,67));
    btnCCARPCGuiMediaControllerQuit->setStyleSheet(QApplication::translate("CCARPCGuiMediaController",
        "     border-style: outset;\n"
        "     border-width: 0px;\n"
        "     border-color: none;\n"
        "     background: none;\n"
    		"", 0, QApplication::UnicodeUTF8));

    btnCCARPCGuiMediaControllerNightMode = new QPushButton(parent);
    btnCCARPCGuiMediaControllerNightMode->setFont(font1);
    btnCCARPCGuiMediaControllerNightMode->setObjectName(QString::fromUtf8("btnCCARPCGuiMediaControllerPlay"));
    btnCCARPCGuiMediaControllerNightMode->setGeometry(QRect(150, iControlYPos, 67, 67));

    btnCCARPCGuiMediaControllerNightMode->setFont(font1);
        QIcon iconVolumeNight;
        iconVolumeNight.addFile ( "picture/nightmode.png", QSize(67,67));
        btnCCARPCGuiMediaControllerNightMode->setIcon(iconVolumeNight);
        btnCCARPCGuiMediaControllerNightMode->setIconSize( QSize(67,67));
        btnCCARPCGuiMediaControllerNightMode->setStyleSheet(QApplication::translate("CCARPCGuiMediaController",
            "     border-style: outset;\n"
            "     border-width: 0px;\n"
            "     border-color: none;\n"
            "     background: none;\n"
        		"", 0, QApplication::UnicodeUTF8));
        btnCCARPCGuiMediaControllerNightMode->setForegroundRole(QPalette::Foreground);






    QFont fontDateTimeLabel;
    fontDateTimeLabel.setFamily(QString::fromUtf8("Comic Sans MS"));
    fontDateTimeLabel.setPointSize(14);
    fontDateTimeLabel.setBold(true);
    fontDateTimeLabel.setWeight(75);



    lbCCARPCGuiMediaControllerTime = new QLabel(parent);
    lbCCARPCGuiMediaControllerTime->setObjectName(QString::fromUtf8("lbTime"));
    lbCCARPCGuiMediaControllerTime->setGeometry(QRect(680, 0, 120, 30));


    lbCCARPCGuiMediaControllerTime->setFont(fontDateTimeLabel);
    lbCCARPCGuiMediaControllerDate = new QLabel(parent);
    lbCCARPCGuiMediaControllerDate->setObjectName(QString::fromUtf8("lbDate"));
    lbCCARPCGuiMediaControllerDate->setGeometry(QRect(0, 0, 140, 30));
    lbCCARPCGuiMediaControllerDate->setFont(fontDateTimeLabel);
    lbCCARPCGuiMediaControllerTime->setText(QApplication::translate("CarPcClass", "    00:00", 0, QApplication::UnicodeUTF8));
    lbCCARPCGuiMediaControllerDate->setText(QApplication::translate("CarPcClass","    01.01.2009", 0, QApplication::UnicodeUTF8));


    lbCCARPCGuiMediaControllerTitle = new QLabel(parent);
    lbCCARPCGuiMediaControllerTitle->setObjectName(QString::fromUtf8("lbTitle"));
    lbCCARPCGuiMediaControllerTitle->setGeometry(QRect(140, 0, 540, 30));
    lbCCARPCGuiMediaControllerTitle->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    lbCCARPCGuiMediaControllerTitle->setFont(fontDateTimeLabel);
    lbCCARPCGuiMediaControllerTitle->setText(cp_MediaPlayer::getInstance()->getArtistTitleasQString());


    lbCCARPCGuiMediaControllerVol = new QLabel(parent);
       lbCCARPCGuiMediaControllerVol->setObjectName(QString::fromUtf8("lbVol"));
       lbCCARPCGuiMediaControllerVol->setGeometry(QRect(690, iControlYPos+55, 50, 30));
       lbCCARPCGuiMediaControllerVol->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
       lbCCARPCGuiMediaControllerVol->setFont(fontDateTimeLabel);
       lbCCARPCGuiMediaControllerVol->setText("30");

    setupStyle();

    UpdateEveryMinute();



	MinTimer = new QTimer(parent);

	connect(btnCCARPCGuiMediaControllerPlay,SIGNAL(clicked()),this,SLOT(play()));
	connect(btnCCARPCGuiMediaControllerSkipB,SIGNAL(clicked()),this,SLOT(skipb()));
	connect(btnCCARPCGuiMediaControllerSkipF,SIGNAL(clicked()),this,SLOT(skipf()));
	connect(btnCCARPCGuiMediaControllerVolumeUp,SIGNAL(clicked()),this,SLOT(volumeup()));
	connect(btnCCARPCGuiMediaControllerVolumeDown,SIGNAL(clicked()),this,SLOT(volumedown()));
	connect(MinTimer,SIGNAL(timeout()),this,SLOT(UpdateEveryMinute()));

	//connect(btnVolumeUp,SIGNAL(clicked()),this, SLOT(VolumeUp()));
	//connect(btnVolumeDown,SIGNAL(clicked()),this, SLOT(VolumeDown()));
	connect(btnCCARPCGuiMediaControllerQuit, SIGNAL(clicked()), parent, SLOT(close()));
	MinTimer->start(1000);





}
void CCARPCGuiMediaController::setupStyle()
{

  QString Style ="border-width: 0px; border-style: solid; border-color: rgba(150, 150, 150, 10%);\nborder-bottom-width: 4px;\nbackground: none;\nbackground-color: rgba(0, 0, 0, 80%);\ncolor:"+ccarpcstyle::getInstance()->getColor()+";";



	 lbCCARPCGuiMediaControllerDate->setStyleSheet(Style);
	 lbCCARPCGuiMediaControllerTime->setStyleSheet(Style);
	 lbCCARPCGuiMediaControllerTitle->setStyleSheet(Style);
	  lbCCARPCGuiMediaControllerVol->setStyleSheet("background: none;\ncolor:"+ccarpcstyle::getInstance()->getColor()+";");
}

void CCARPCGuiMediaController::UpdateEveryMinute()
{
	CLog::getInstance()->Log(QString("CarPc::UpdateEveryMinute()"),LOG_DEBUGDEVELOP);
	QTime * Time = new QTime(QTime::currentTime());
	lbCCARPCGuiMediaControllerTime->setText("   "+Time->toString("hh:mm"));
	QDate * Date = new QDate(QDate::currentDate());
	lbCCARPCGuiMediaControllerDate->setText("   "+Date->toString("dd.MM.yyyy"));
	lbCCARPCGuiMediaControllerTitle->setText(cp_MediaPlayer::getInstance()->getArtistTitleasQString());
	lbCCARPCGuiMediaControllerVol->setText(QString::number((int)cp_MediaPlayer::getInstance()->getVolume(),10));
}

void CCARPCGuiMediaController::play()
{
  cp_MediaPlayer::getInstance()->Play();
}

void CCARPCGuiMediaController::skipf()
{
  cp_MediaPlayer::getInstance()->SkipForward();
}

void CCARPCGuiMediaController::skipb()
{
  cp_MediaPlayer::getInstance()->SkipBack();
}

void CCARPCGuiMediaController::volumeup()
{
	bool ok = false;
	int iVolumesteps = CSettings::getInstance()->getSetting("volumesteps").toInt(&ok,10);
	cp_MediaPlayer::getInstance()->VolumeUp(iVolumesteps);
	lbCCARPCGuiMediaControllerVol->setText(QString::number(cp_MediaPlayer::getInstance()->getVolume(),10));
}

void CCARPCGuiMediaController::volumedown()
{
	bool ok = false;
	int iVolumesteps = CSettings::getInstance()->getSetting("volumesteps").toInt(&ok,10);
	cp_MediaPlayer::getInstance()->VolumeDown(iVolumesteps);
	lbCCARPCGuiMediaControllerVol->setText(QString::number(cp_MediaPlayer::getInstance()->getVolume(),10));
}
