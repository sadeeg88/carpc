/*
 * cp_dlgMusikMenu.h
 *
 *  Created on: 22.02.2011
 *      Author: Sascha
 */
#include "cp_dlgStdMenu.h"

#ifndef CP_DLGMUSIKMENU_H_
#define CP_DLGMUSIKMENU_H_

class cp_dlgMusikMenu : public cp_dlgStdMenu
{
  Q_OBJECT;
public:
  cp_dlgMusikMenu(QWidget* parent);
  virtual
  ~cp_dlgMusikMenu();
private slots:
  void DoAction(QString strItemName);
};

#endif /* CP_DLGMUSIKMENU_H_ */
