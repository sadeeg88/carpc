/*
 * cp_dlgStdMenu.h
 *
 *  Created on: 04.02.2011
 *      Author: Sascha
 */
#include <QtGui/QDialog>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <qapplication.h>
#include "../gui/CCARPCGuiMediaController.h"

#ifndef CP_DLGSTDMENU_H_
#define CP_DLGSTDMENU_H_

class cp_menu_item
{
  public:
  QString m_strName;
  QIcon m_Icon;
  QString m_strDescription;

  cp_menu_item()
  {

  }
  cp_menu_item(QString strName,QIcon IQIcon ,QString strDescription)
  {
    m_strName = strName;
    m_Icon = IQIcon;
    m_strDescription = strDescription;
  }

  friend  bool operator==(cp_menu_item const& lhs, cp_menu_item const& rhs)
  {
     if(lhs.m_strName.compare(rhs.m_strName) == 0)
     {
       if(lhs.m_strDescription.compare(rhs.m_strDescription) == 0)
       {
         //if(lhs.m_Icon == rhs.m_Icon)
          //{
               return true;
          //}

       }

     }
     return false;
   }

  friend  bool operator != (cp_menu_item const& lhs, cp_menu_item const& rhs)
      {
         if(lhs.m_strName.compare(rhs.m_strName) == 0)
         {
           if(lhs.m_strDescription.compare(rhs.m_strDescription) == 0)
           {
             //if(lhs.m_Icon == rhs.m_Icon)
              //{
                   return false;
              //}

           }

         }
         return true;
       }


};

class cp_dlgStdMenu : public QDialog
{
  Q_OBJECT;
public:
  cp_dlgStdMenu(QWidget *parent);
  virtual
  ~cp_dlgStdMenu();
protected:
  CCARPCGuiMediaController * pGuiMediaController;
  QPushButton* btnSelect;
  QPushButton* btnSwitchRight;
  QPushButton* btnSwitchLeft;
  QLabel * lbLabel;
  QList <cp_menu_item> Items;
  void SetupMenu();
  void ResetMenuItems();
  void AddMenuItem(QString strName,QString strIcon, QString strDescription);
  cp_menu_item CurrentItem;
  void setupStyle();

private slots:
  void ItemAction();
public slots:
   void NextItem();
   void PreviosItem();
   void Nightmode();


   signals:
   void ItemSelected(QString strItemName);

};

#endif /* CP_DLGSTDMENU_H_ */
