/*
 * ccarpcstyle.cpp
 *
 *  Created on: 08.07.2010
 *      Author: Sascha
 */

#include "ccarpcstyle.h"

ccarpcstyle::ccarpcstyle() {
	// TODO Auto-generated constructor stub
	bNightmode = false;

}

ccarpcstyle* ccarpcstyle::ccarpcstyle_instance = NULL;

ccarpcstyle::~ccarpcstyle()
{


}



ccarpcstyle* ccarpcstyle::getInstance()
{
  if( !ccarpcstyle_instance )
	  ccarpcstyle_instance = new ccarpcstyle();
  return ccarpcstyle_instance;
}

void ccarpcstyle::Destroy()
{
  // static
  if (ccarpcstyle_instance){
    delete ccarpcstyle_instance;
    ccarpcstyle_instance = NULL;
  }
}

QString ccarpcstyle::getColor()
{
  QString strColor;
	if(bNightmode)
	{
	      strColor = CSettings::getInstance()->getSetting("Color-Night");
	      if(strColor.compare("")==0)
	      {
	        strColor = "blue";
	       }
	}
	else
	{
          strColor = CSettings::getInstance()->getSetting("Color-Day");
            if(strColor.compare("")==0)
            {
              strColor = "blue";
            }
	}
	return strColor;
}

QString ccarpcstyle::getDialogStyle()
{
  QString strStyle,strColor,BackgroundColor,BackgroundImage;

  if(bNightmode)
    {
          strColor = CSettings::getInstance()->getSetting("Color-Night");
          if(strColor.compare("")==0)
          {
            strColor = "blue";
           }

          BackgroundColor = CSettings::getInstance()->getSetting("Background-Color-Night");
           if(BackgroundColor.compare("")==0)
           {
             BackgroundColor = "black";
           }
           BackgroundImage = CSettings::getInstance()->getSetting("Background-Image-Night");
            if(BackgroundImage.compare("")==0)
            {
              BackgroundImage = "none";
            }
    }
    else
    {
      strColor = CSettings::getInstance()->getSetting("Color-Day");
      if(strColor.compare("")==0)
      {
        strColor = "blue";
       }

      BackgroundColor = CSettings::getInstance()->getSetting("Background-Color-Day");
       if(BackgroundColor.compare("")==0)
       {
         BackgroundColor = "black";
       }
       BackgroundImage = CSettings::getInstance()->getSetting("Background-Image-Day");
        if(BackgroundImage.compare("")==0)
        {
          BackgroundImage = "none";
        }
    }
  strStyle = "background-color:"+BackgroundColor+";\n";
  strStyle += "color:"+strColor+";\n";
  if(BackgroundImage.compare("none") == 0)
    {
     strStyle = "background: none;\n"+strStyle;
    }
  else
    {
    strStyle +=" background-image: url("+BackgroundImage+");\n";
    }

     // "background-image: url(picture/background.png);\n"

  return strStyle;
}

void ccarpcstyle::setNightMode(bool ngMode)
{
	   bNightmode = ngMode;
}

bool ccarpcstyle::getNightMode()
{
	   return bNightmode;
}
