#ifndef cp_dlgVideo_h
#define cp_dlgVideo_H

#include <QtGui/QWidget>
#include "../ui_cp_dlgVideo.h"
#include "../gui/cp_videowidget.h"
#include "CCARPCGuiMediaController.h"
#include <path.h>
#include <videowidget.h>
#include <seekslider.h>

class cp_dlgVideo : public QDialog, private Ui_cp_dlgVideo
{
    Q_OBJECT

public:
    cp_dlgVideo(QWidget *parent = 0);
    ~cp_dlgVideo();
private:
    CCARPCGuiMediaController * pGuiMediaController;

    cp_videowidget *videoWidget;
    Phonon::SeekSlider * seeksilder;
    Phonon::Path  pathvideo;
    void setupStyle();
public slots:
    void FullscreenMode();
    void Nightmode();


};

#endif // CARPC_H
