/*
 * CCARPCGuiMediaController.h
 *
 *  Created on: 09.12.2009
 *      Author: Sascha
 */

#ifndef CCARPCGUIMEDIACONTROLLER_H_
#define CCARPCGUIMEDIACONTROLLER_H_

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLCDNumber>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>
#include <QIcon>

class CCARPCGuiMediaController : public QObject{
	Q_OBJECT
public:
	CCARPCGuiMediaController(QWidget *parent);
	virtual ~CCARPCGuiMediaController();
	void setupMediaplayer(QWidget *parent);
	void setupStyle();
	QPushButton  * btnCCARPCGuiMediaControllerPlay;
	QPushButton  * btnCCARPCGuiMediaControllerSkipF;
	QPushButton  * btnCCARPCGuiMediaControllerSkipB;
	QPushButton  * btnCCARPCGuiMediaControllerVolumeUp;
	QPushButton  * btnCCARPCGuiMediaControllerVolumeDown;
	QPushButton  * btnCCARPCGuiMediaControllerQuit;
	QPushButton  * btnCCARPCGuiMediaControllerNightMode;
	QLabel * lblbCCARPCGuiMediaControllerBackgroundButton;
	QLabel * lbCCARPCGuiMediaControllerDate;
	QLabel * lbCCARPCGuiMediaControllerTime;
	QLabel * lbCCARPCGuiMediaControllerTitle;
	QLabel * lbCCARPCGuiMediaControllerVol;
	QTimer * MinTimer;


public slots:
	void play();
	void skipf();
	void skipb();
	void volumeup();
	void volumedown();
	virtual void UpdateEveryMinute();
};

#endif /* CCARPCGUIMEDIACONTROLLER_H_ */
