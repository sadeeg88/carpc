#ifndef CMUSIKSELECT_H
#define CMUSIKSELECT_H

#include <QtGui/QDialog>
#include "ui_cmusikselect.h"
#include "CCARPCGuiMediaController.h"

class CMusikSelect : public QDialog, private Ui_CMusikSelectClass
{
    Q_OBJECT;

public:
    CMusikSelect(QWidget *parent = 0);
    ~CMusikSelect();
private:
    CCARPCGuiMediaController * pGuiMediaController;
    void setupStyle();
public slots:
	void MediaAlbum();
	void MediaInterpret();
	void Nightmode();
	void PlayCD();

};

#endif // CMUSIKSELECT_H
