/*
 * ccarpctouchkeyboard_context.cpp
 *
 *  Created on: 24.08.2010
 *      Author: Sascha
 */

#include "ccarpctouchkeyboard_context.h"
#include <qpointer.h>
#include <qwidget.h>

ccarpctouchkeyboard_context::ccarpctouchkeyboard_context()
{
    inputPanel = new ccarpctouchkeyboard();
    connect(inputPanel, SIGNAL(characterGenerated(QChar)), SLOT(sendCharacter(QChar)));
}

ccarpctouchkeyboard_context::~ccarpctouchkeyboard_context() {
	delete inputPanel;
	inputPanel = NULL;
}

bool ccarpctouchkeyboard_context::filterEvent(const QEvent* event)
{
    if (event->type() == QEvent::RequestSoftwareInputPanel) {
        updatePosition();
        inputPanel->show();
        return true;
    } else if (event->type() == QEvent::CloseSoftwareInputPanel) {
        inputPanel->hide();
        return true;
    }
    return false;
}

//! [1]

QString ccarpctouchkeyboard_context::identifierName()
{
    return "ccarpctouchkeyboard_context";
}

void ccarpctouchkeyboard_context::reset()
{
}

bool ccarpctouchkeyboard_context::isComposing() const
{
    return false;
}

QString ccarpctouchkeyboard_context::language()
{
    return "en_US";
}

//! [2]

void ccarpctouchkeyboard_context::sendCharacter(QChar character)
{
    QPointer<QWidget> w = focusWidget();
    if(character == '\b')
      {
      QKeyEvent keyPress(QEvent::KeyPress, Qt::Key_Backspace,/*Qt::ControlModifier | Qt::MetaModifier | Qt::ShiftModifier*/Qt::NoModifier, QString());
      QApplication::sendEvent(w, &keyPress);

      }
    else
      {
    if (!w)
        return;

    QKeyEvent keyPress(QEvent::KeyPress, character.unicode(), Qt::NoModifier, QString(character));
    QApplication::sendEvent(w, &keyPress);

    if (!w)
        return;

    QKeyEvent keyRelease(QEvent::KeyPress, character.unicode(), Qt::NoModifier, QString());
    QApplication::sendEvent(w, &keyRelease);
      }
}

//! [2]

//! [3]

void ccarpctouchkeyboard_context::updatePosition()
{
    QWidget *widget = focusWidget();
    if (!widget)
        return;

    QRect widgetRect = widget->parentWidget()->geometry();
    QPoint panelPos =QPoint(widgetRect.x(),widgetRect.y());
    panelPos = widget->mapToGlobal(panelPos);
    QRect inwidgetRect(widgetRect.x(),widgetRect.y()+ 600-200,800,200);
    //inwidgetRect.setX(widgetRect.x());
    //inwidgetRect.setY());
    inputPanel->setGeometry(inwidgetRect);
}

//! [3]

