#include "../gui/cp_dlgVideo.h"
#include <QPixmap>
#include <QTime>
#include <QTimer>
#include <QDate>
#include <phonon/phonon>
#include "../basis/CLog.h"
#include "../multimedia/cp_MediaPlayer.h"
#include "../gui/cmusikselect.h"
#include "../basis/CSettings.h"
#include "../gui/ccarpcstyle.h"
#include "../gui/ccarpctouchkeyboard.h"
#include "../basis/CCARPCDBConnection.h"



//#include "stdio.h"

 
cp_dlgVideo::cp_dlgVideo(QWidget *parent)
    : QDialog(parent)
{
	CLog::getInstance()->Log(QString("cp_dlgVideo::cp_dlgVideo(QWidget *parent)"),LOG_DEBUGDEVELOP);
	setupUi(this);



	this->setMinimumSize(QSize(10,10));
	videoWidget = new cp_videowidget(this);

	videoWidget->setGeometry(QRect(0, 50, 800, 400));
	pathvideo = Phonon::createPath(cp_MediaPlayer::getInstance()->m_pMediaObjekt, videoWidget);



	seeksilder = new Phonon::SeekSlider(this);
	seeksilder->setGeometry(QRect(0, 450, 800, 50));
	seeksilder->setMediaObject(cp_MediaPlayer::getInstance()->m_pMediaObjekt);

        if(CSettings::getInstance()->getSetting("Widescreen").compare("true") == 0)
         {
           this->resize(800,480);
           videoWidget->setGeometry(QRect(0, 50, 800, 400-120));
           seeksilder->setGeometry(QRect(0, 450-120, 800, 50));
         }
         else
         {
           this->resize(800,600);
           videoWidget->setGeometry(QRect(0, 50, 800, 400));
           seeksilder->setGeometry(QRect(0, 450, 800, 50));

         }
        pGuiMediaController = new CCARPCGuiMediaController(this);
        pGuiMediaController->setupMediaplayer(this);



	if(CSettings::getInstance()->getSetting("FullScreen").compare("true") == 0)
	{
	        this->showFullScreen();
	        this->move(0,0);
	}
	setupStyle();
	this->setStyleSheet(ccarpcstyle::getInstance()->getDialogStyle());
	connect(pGuiMediaController->btnCCARPCGuiMediaControllerNightMode,SIGNAL(clicked()),this, SLOT(Nightmode()));
	        connect(videoWidget,SIGNAL(clicked()),this, SLOT(FullscreenMode()));


}

cp_dlgVideo::~cp_dlgVideo()
{
      cp_MediaPlayer::getInstance()->clear();
        if(!pathvideo.disconnect())
        {
          pathvideo.disconnect();
        }
	CLog::getInstance()->Log(QString("CarPc::~CarPc()"),LOG_DEBUGDEVELOP);
	delete videoWidget;
	videoWidget = NULL;


}


void cp_dlgVideo::setupStyle()
{
  this->setStyleSheet(ccarpcstyle::getInstance()->getDialogStyle());
  pGuiMediaController->setupStyle();

}

void cp_dlgVideo::Nightmode()
{
        ccarpcstyle::getInstance()->setNightMode(!ccarpcstyle::getInstance()->getNightMode());
        setupStyle();
}

void cp_dlgVideo::FullscreenMode()
{
  if(videoWidget->isFullScreen())
  {
    videoWidget->setFullScreen(false);
    if(CSettings::getInstance()->getSetting("Widescreen").compare("true") == 0)
     {
       videoWidget->setGeometry(QRect(0, 50, 800, 400-120));
     }
     else
     {
       videoWidget->setGeometry(QRect(0, 50, 800, 400));
     }

  }
  else
  {
    videoWidget->setFullScreen(true);
  }

}
