/*
 * cp_dlgNavit.h
 *
 *  Created on: 25.07.2011
 *      Author: sascha
 */

#ifndef CP_DLGNAVIT_H_
#define CP_DLGNAVIT_H_

#include <QtGui/QDialog>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <qapplication.h>
#include "../gui/CCARPCGuiMediaController.h"
#include <qx11embed_x11.h>
#include <qprocess.h>

class cp_dlgNavit: public QDialog
{
  Q_OBJECT;
private:
	static cp_dlgNavit * instance;
	cp_dlgNavit(QWidget *parent);
	virtual ~cp_dlgNavit();
	void setupStyle();

public:
	   static cp_dlgNavit * getInstance(QWidget *parent);
	   static void Destroy();
	   static bool Exsist();
protected:
  CCARPCGuiMediaController * pGuiMediaController;
  QX11EmbedContainer * container;
  QProcess * process;
  public slots:
	  void navit_start();
	  void Nightmode();
};

#endif /* CP_DLGNAVIT_H_ */
