#ifndef CARPC_H
#define CARPC_H

#include <QtGui/QWidget>
#include "ui_cccarpc_main_dialog.h"
#include "CCARPCGuiMediaController.h"

class ccarpc_dlgMain : public QWidget, private Ui_ccarpc_dlgMain
{
    Q_OBJECT

public:
    ccarpc_dlgMain(QWidget *parent = 0);
    ~ccarpc_dlgMain();
private:
    void loadLogo();
    void setupStyle();

    CCARPCGuiMediaController * pGuiMediaController;
public slots:
	void VolumeUp();
	void VolumeDown();
	void GoToMSelect();
	void clickedVideo();
	void Nightmode();


};

#endif // CARPC_H
