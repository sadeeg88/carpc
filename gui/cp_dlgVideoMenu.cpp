/*
 * cp_dlgVideoMenu.cpp
 *
 *  Created on: 05.02.2011
 *      Author: Sascha
 */

#include "cp_dlgVideoMenu.h"
#include "../gui/cp_dlgVideo.h"
#include "../gui/ccarpcplaylist.h"
#include "../multimedia/cp_MediaPlayer.h"

cp_dlgVideoMenu::cp_dlgVideoMenu(QWidget * parent)
:cp_dlgStdMenu(parent)
{
   //this->AddMenuItem("Video","picture/video.png","Anzeige");
   this->AddMenuItem("VideoBibliothek","picture/videobib.png","Bibliothek");
   this->AddMenuItem("DVD","picture/dvd.png","DVD");
   SetupMenu();
   connect(this,SIGNAL(ItemSelected(QString)),this, SLOT(DoAction(QString)));

}

cp_dlgVideoMenu::~cp_dlgVideoMenu()
{
  // TODO Auto-generated destructor stub
}

void cp_dlgVideoMenu::DoAction(QString strItemName)
{
  if(strItemName.compare("Video")==0)
    {
    cp_dlgVideo * dlgVideo = new cp_dlgVideo(this);
    dlgVideo->show();
    dlgVideo->exec();
    delete dlgVideo;
    /*setupStyle();*/

    }
  else if (strItemName.compare("DVD")==0)
    {
    QStringList Dummy;
    cp_MediaPlayer::getInstance()->setPlayList(Dummy,1,DVD);
    cp_dlgVideo * dlgVideo = new cp_dlgVideo(this);
    dlgVideo->show();
    dlgVideo->exec();
    delete dlgVideo;
    }

  else if (strItemName.compare("VideoBibliothek")==0)
    {
    CCARPCPlaylist * mpDialog = new CCARPCPlaylist(this,CCARPC_PLAYLIST_MOVIES);
          mpDialog->show();
          mpDialog->exec();
          delete mpDialog;
          //setupStyle();

    }

}
