/*
 * cp_dlgMainMenu.cpp
 *
 *  Created on: 20.02.2011
 *      Author: Sascha
 */

#include "cp_dlgSettingMenu.h"
#include "../multimedia/cp_MediaBibliothek.h"
#include <qmessagebox.h>
#include "../gui/cp_dlgNavit.h"


cp_dlgSettingMenu::cp_dlgSettingMenu(QWidget * parent)
:cp_dlgStdMenu(parent)
{
   this->AddMenuItem("LADEMUSIK","picture/cp_musicmenu_bibliothek.png","Reload Mediabibliothek");
   this->AddMenuItem("COPYUSB","picture/cp_settingmenu_copy.png","USB -> HDD kopieren");
#ifdef Q_WS_X11
   if(cp_dlgNavit::Exsist())
   {
	   this->AddMenuItem("NAVIT","picture/dvd.png","Navigation beeenden");
   }
#endif
   SetupMenu();
   connect(this,SIGNAL(ItemSelected(QString)),this, SLOT(DoAction(QString)));



}

cp_dlgSettingMenu::~cp_dlgSettingMenu()
{
  // TODO Auto-generated destructor stub
}

void cp_dlgSettingMenu::DoAction(QString strItemName)
{
  if(strItemName.compare("LADEMUSIK")==0)
    {
      QMessageBox msgBox;
      msgBox.setText("Reload Mediabibliothek");
      msgBox.setInformativeText("Please Wait.....");
      msgBox.setStandardButtons(QMessageBox::Ok);
      msgBox.showNormal();
      msgBox.setFocus();
      cp_MediaBibliothek::getInstance()->LoadMusikFileinMediaBibliothek();


    }
#ifdef Q_WS_X11
 else if (strItemName.compare("NAVIT")==0)
 {
	 cp_dlgNavit::Destroy();
 }
#endif

 /* else if (strItemName.compare("VideoBibliothek")==0)
    {
    CCARPCPlaylist * mpDialog = new CCARPCPlaylist(this,CCARPC_PLAYLIST_MOVIES);
          mpDialog->show();
          mpDialog->exec();
          delete mpDialog;
          //setupStyle();

    }*/

}
