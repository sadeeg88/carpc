/*
 * ccarpctouchkeyboard.cpp
 *
 *  Created on: 07.08.2010
 *      Author: Sascha
 */

#include "ccarpctouchkeyboard.h"
#include "../gui/ccarpcstyle.h"
#include <qvariant.h>


ccarpctouchkeyboard::ccarpctouchkeyboard()
: QWidget(0, Qt::Tool | Qt::WindowStaysOnTopHint),
  lastFocusedWidget(0)
{
  bUpperCase = false;
  iStatus=1;

    connect(qApp, SIGNAL(focusChanged(QWidget*,QWidget*)),
            this, SLOT(saveFocusWidget(QWidget*,QWidget*)));

	this->setGeometry(QRect(0,400,800,200));
	this->setStyleSheet("color:"+ccarpcstyle::getInstance()->getColor()+";");
	QString Zeichen ="";
	strTastenZeichen[0] = QString("qwertzuiop�asdfghjkl��yxcvbnm").toUpper();
	strTastenZeichen[1] = QString("1234567890-/:;()E&@\".,?!����[");
	strTastenZeichen[2] = QString("[]{}#%^*+=_\\|~<>$�'��,.?!���&");

	 Zeichen = strTastenZeichen[0];
	int vPos = 10;
	int hPos = 10;
	int iz = 0;
	int KeyCount = 0;

	for(int x = 0; x < 35; x++)
	{
		keys[x] = new QPushButton(this);

		if(x == 34)
		{
		keys[x]->setGeometry(QRect(vPos,hPos,110,50));
		keys[x]->setObjectName("Key"+QString::number(x,10));
		keys[x]->setText( QString("Close"));
		connect(keys[x],SIGNAL(clicked()),this, SLOT(hide()));
		}
		else if(x == 26)
	        {
	                  keys[x]->setGeometry(QRect(vPos,hPos,50,50));
	                  keys[x]->setObjectName("Key"+QString::number(x,10));
	                  keys[x]->setText( QString("/\\"));
	                  connect(keys[x],SIGNAL(clicked()),this, SLOT(UpperCase()));
	        }
		else if(x == 30)
		{
		   keys[x]->setGeometry(QRect(vPos,hPos,50,50));
		   keys[x]->setObjectName("Key"+QString::number(x,10));
		   keys[x]->setText( QString("123"));
		   connect(keys[x],SIGNAL(clicked()),this, SLOT(ChangeKeys()));
		                }
		else if(x == 32)
		{
		   keys[x]->setGeometry(QRect(vPos,hPos,110,50));
		   keys[x]->setObjectName("Key"+QString::number(x,10));
		   keys[x]->setText( QString("Enter"));
		   keys[x]->setProperty("ButtonValue",'\n');
		     connect(keys[x], SIGNAL(clicked()), &signalMapper, SLOT(map()));
		 }
	         else if(x == 31)
		                        {
		                        keys[x]->setGeometry(QRect(vPos,hPos,170,50));
		                        keys[x]->setObjectName("Key"+QString::number(x,10));
		                        keys[x]->setText( QString(""));
		                        keys[x]->setProperty("ButtonValue",' ');
		                        signalMapper.setMapping(keys[x],keys[x]);
		                        connect(keys[x], SIGNAL(clicked()),
		                                     &signalMapper, SLOT(map()));
		                        }
		else if(x == 33)
	                {
	                keys[x]->setGeometry(QRect(vPos,hPos,50,50));
	                keys[x]->setObjectName("Key"+QString::number(x,10));
	                keys[x]->setText( QString("Del"));
	                keys[x]->setProperty("ButtonValue",'\b');
	                signalMapper.setMapping(keys[x],keys[x]);
	                connect(keys[x], SIGNAL(clicked()),
	                             &signalMapper, SLOT(map()));
	                }
		else
		{
			keys[x]->setGeometry(QRect(vPos,hPos,50,50));
			keys[x]->setObjectName("Key"+QString::number(x,10));
			keys[x]->setText( QString(Zeichen.at(KeyCount)));
			keys[x]->setProperty("ButtonValue",Zeichen.at(KeyCount));
			signalMapper.setMapping(keys[x],keys[x]);
			connect(keys[x], SIGNAL(clicked()),
			            &signalMapper, SLOT(map()));
			KeyCount++;
		}
		keys[x]->setFocusPolicy(Qt::NoFocus);
		keys[x]->setStyleSheet("font: 20pt \"Comic Sans MS\";\nbackground: none;\n");

		if(x==31)
		  vPos = vPos + 180;
		else if (x==32)
		  vPos = vPos + 120;
		else
		vPos = vPos +60;

		iz++;

		if(iz > 12)
		{
			iz = 0;
			vPos = 10;
			hPos = hPos +60;
		}


	}
	this->setStyleSheet(ccarpcstyle::getInstance()->getDialogStyle());
	this->setWindowFlags(this->windowType()|Qt::FramelessWindowHint);

    connect(&signalMapper, SIGNAL(mapped(QWidget*)),
            this, SLOT(buttonClicked(QWidget*)));



}

ccarpctouchkeyboard::~ccarpctouchkeyboard() {
	// TODO Auto-generated destructor stub
}

bool ccarpctouchkeyboard::event(QEvent *e)
{
    switch (e->type()) {
//! [1]
    case QEvent::WindowActivate:
        if (lastFocusedWidget)
            lastFocusedWidget->activateWindow();
        break;
//! [1]
    default:
        break;
    }

    return QWidget::event(e);
}

//! [2]

void ccarpctouchkeyboard::saveFocusWidget(QWidget * /*oldFocus*/, QWidget *newFocus)
{
    if (newFocus != 0 && !this->isAncestorOf(newFocus)) {
        lastFocusedWidget = newFocus;
    }
}

//! [2]

//! [3]

void ccarpctouchkeyboard::buttonClicked(QWidget *w)
{
    QChar chr = qvariant_cast<QChar>(w->property("ButtonValue"));
    if(!bUpperCase)
      chr = chr.toLower();
    if(chr == '\r')
      {
      emit characterGenerated(8);
      }
    else
      {
    emit characterGenerated(chr);
      }
    if(bUpperCase)
      UpperCase();
 }

//! [3]

void  ccarpctouchkeyboard::UpperCase()
{
  if(bUpperCase)
  {
    keys[26]->setText( QString("/\\"));
    bUpperCase = false;
  }
  else
  {
    keys[26]->setText( QString("\\/"));
    bUpperCase = true;
  }
}

void ccarpctouchkeyboard::ChangeKeys()
{
  QString AktZeichen;
  if(bUpperCase)
    UpperCase();

  if(iStatus ==1)
  {
    iStatus = 2;
    AktZeichen = strTastenZeichen[1];
    keys[26]->setDisabled(true);
    keys[30]->setText(QString("#+="));
  }
  else if (iStatus == 2)
  {
    iStatus = 3;
    AktZeichen = strTastenZeichen[2];
    keys[26]->setDisabled(true);
    keys[30]->setText(QString("ABC"));
  }
  else
  {
    iStatus = 1;
    AktZeichen = strTastenZeichen[0];
    keys[26]->setDisabled(false);
    keys[30]->setText(QString("123"));
  }

  int KeyCount = 0;

    for(int x = 0; x < 35; x++)
    {
      if(!(x == 26||x==30||x==31||x==32||x==33||x==34))
      {
        keys[x]->setText( QString(AktZeichen.at(KeyCount)));
        keys[x]->setProperty("ButtonValue",AktZeichen.at(KeyCount));
        KeyCount++;
      }

    }

}
