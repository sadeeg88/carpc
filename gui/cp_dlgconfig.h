#ifndef CP_DLGCONFIG_H
#define CP_DLGCONFIG_H

#include <QtGui/QDialog>
#include "ui_cp_dlgconfig.h"

class cp_dlgConfig : public QDialog
{
    Q_OBJECT

public:
    cp_dlgConfig(QWidget *parent = 0);
    ~cp_dlgConfig();

private:
    Ui::cp_dlgConfigClass ui;
};

#endif // CP_DLGCONFIG_H
